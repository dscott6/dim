subroutine parameter_setting &
(Hdrop,Rdrop,l,m,n,Lx,Ly,Lz,dx,dy,dz,dt,Re,Pr,Ja,Sc,Pe,epn,We,Fr,Bod,rGd,rGv,rGk,rGcp,rGb,rOd,rOv,rOk,rOcp,rOb,rMW,Pabs,psA,psB,psC)

	implicit none
	
	integer, 		intent(in)	::	l,m,n

	double precision,	intent(out)	::	Hdrop,Rdrop
	double precision,	intent(out)	::	Lx,Ly,Lz
	double precision,	intent(out)	::	dx,dy,dz,dt
	double precision,	intent(out)	::	Re,Pr,Ja,Sc,Pe,epn,We,Fr,Bod
	double precision,	intent(out)	::	rGd,rGv,rGk,rGcp,rGb
	double precision,	intent(out)	::	rOd,rOv,rOk,rOcp,rOb
	double precision,	intent(out)	::	rMW,Pabs,psA,psB,psC

	double precision			::	xi
	

	! Time step
	! ----------------------------------
	dt = 0.01d0

	! Geometry
	! ----------------------------------
	Hdrop = 1.1d0
	Rdrop = 1.2d0
	xi = Hdrop/Rdrop

	! Spherical droplet (complete sphere)
	Lx = 6.d0*Rdrop
	Ly = Lx
	Lz = 3.d0*Rdrop

! 	! Sessile droplet (spherical cap)
! 	Lx = 2.d0*(2.d0*Rdrop)
! 	Ly = 2.d0*(2.d0*Rdrop)
! 	Lz = 2.d0*Hdrop

	dx = Lx/(l-1)
	dy = Ly/(m-1)
	dz = Lz/(n-1)

	! Dimensionless groups
	! ----------------------------------
	Re  = 12905.d0	*0.d0 + 13000.d0
 	Pr  =  6.132d0	*0.d0 + 6.d0
 	Ja  =  6.881d-2	*0.d0 + 0.07d0
	Sc  =  3.544d-2	*0.d0 + 0.035d0
	epn = 0.5d0*dmin1(dx,dy,dz)
	Pe  = 1.d0/(epn*epn)
	We  = 143.1d0	*0.d0 + 140.d0
	Fr  = 4011.d0	*0.d0 + 10000000*4000.d0
	Bod = 3.307d-2	*0.d0 !+ 0.03d0
	
	rGd  = 870.7d0	*0.d0 + 870.d0
	rGv  = 51.61d0	*0.d0 + 50.d0
	rGk  = 23.66d0	*0.d0 + 25.d0
 	rGcp = 4.023d0	*0.d0 + 4.d0
	rGb  = 0.07649d0*0.d0 + 0.08d0
	
	rOd  = 1354.d0	*0.d0 + 1350.d0
	rOv  = 89.3d0 	*0.d0 + 90.d0
	rOk  = 32.63d0	*0.d0 + 30.d0
	rOcp = 2.242d0	*0.d0 + 2.d0
	rOb  = 0.07649d0*0.d0 + 0.08d0

	!Vapor pressure
	! ----------------------------------
	rMW   = 0.6433d0
	Pabs  = 0.5808d0

	! These parameters depend on p0, DelT and Tr
	! They need to be updated for every new run
	! See notes "Dimensional analysis" Page DA11)
	psA = 5.411d0
	psB = 43.09d0
	psC = 6.452d0

	! Output data
	! ----------------------------------
	write(6,100) 'PARAMETER SETTING'
	write(6,102) 'l  = ',l,'m  = ',m,'n  = ',n
	write(6,101) 'e  = ',xi,'H  = ',Hdrop,'R  = ',Rdrop,'Lx = ',Lx,'Ly = ',Ly,'Lz = ',Lz
	write(6,101) 'dx = ',dx,'dy = ',dy,'dz = ',dz,'dt = ',dt
	write(6,101) 'Re  = ',Re,'Pr  = ',Pr,'Ja  = ',Ja,'Sc  = ',Sc,'Pe  = ',Pe,'epn = ',epn,'We  = ',We,'Fr  = ',Fr,'Bod = ',Bod
	write(6,101) 'rGd  = ',rGd,'rGv  = ',rGv,'rGk  = ',rGk,'rGcp = ',rGcp,'rGb  = ',rGb
	write(6,101) 'rOd  = ',rOd,'rOv  = ',rOv,'rOk  = ',rOk,'rOcp = ',rOcp,'rOb  = ',rOb
	write(6,101) 'rMW  = ',rMW,'Pabs = ',Pabs,'psA  = ',psA,'psB  = ',psB,'psC  = ',psC


100   format(/,a,/)      
101   format(10(4x,a,ES20.8E2,/))      
102   format(3(4x,a,I5,/))      
	
return
end subroutine parameter_setting

! ###########################################################################################
! ###########################################################################################

subroutine data_load &
(l,m,n,dx,dy,dz,Hdrop,Rdrop,Lx,Ly,Lz,epn,Fr,rGd,rOd,psA,psB,psC,rMW,Pabs,fi,fiold,ConvPrevC,SpMF,SpMFold,ConvPrevS,T,Told,ConvPrevT,u2,v2,w2,preU,preV,preW,p,prevIt)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz

	double precision,	intent(in)	::	Hdrop,Rdrop
	double precision,	intent(in)	::	Lx,Ly,Lz
	double precision,	intent(in)	::	epn,Fr,rGd,rOd
	double precision,	intent(in)	::	psA,psB,psC,rMW,Pabs

	double precision,	intent(out)	::	fi	 (l+1,m+1,n+1)
	double precision,	intent(out)	::	fiold    (l+1,m+1,n+1)
	double precision,	intent(out)	::	ConvPrevC(l+1,m+1,n+1)

	double precision,	intent(out)	::	SpMF     (l+1,m+1,n+1)
	double precision,	intent(out)	::	SpMFold  (l+1,m+1,n+1)
	double precision,	intent(out)	::	ConvPrevS(l+1,m+1,n+1)

	double precision,	intent(out)	::	T   	 (l+1,m+1,n+1)
	double precision,	intent(out)	::	Told   	 (l+1,m+1,n+1)
	double precision,	intent(out)	::	ConvPrevT(l+1,m+1,n+1)

 	double precision,	intent(out)	::	u2(l  ,m-1,n-1)
 	double precision,	intent(out)	::	v2(l-1,m  ,n-1)
 	double precision,	intent(out)	::	w2(l-1,m-1,n  )

	double precision,	intent(out)	::	preU(l  ,m-1,n-1)
 	double precision,	intent(out)	::	preV(l-1,m  ,n-1)
 	double precision,	intent(out)	::	preW(l-1,m-1,n  )

 	double precision,	intent(out)	::	p(l+1,m+1,n+1)

	integer,		intent(out)	::	prevIt

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	logical 				:: 	dir_e,newrun
	character(len=60)			::	lastbak,filename,temp

	integer 				::	i,j,k
	double precision			::	x,y,z,Rs,rp,delr,p1,psat,rho



! +======================================+
! | CALCULATIONS			 |
! +======================================+
      
write(6,100) "INITIALIZING THE FLOW..."


! Check whether the simulation is being run for the first time or not
INQUIRE(DIRECTORY='results', EXIST=dir_e)

if(dir_e)then

	write(6,101) "Job continued from the last result file."
	
else
	write(6,101) "Job running for the first time."
	call system('mkdir results')
	
endif

newrun = .NOT.(dir_e)
	

! +==============================================================+
! | INITIAL CONDITIONS - Simulation running for the first time   |	
! +==============================================================+

if(newrun)then
	
	prevIt = 0

	write(6,101) "Loading initial condtions..."

	do i = 1,l+1
		do j = 1,m+1
			do k = 1,n+1
	
				x = -(Lx + dx)/2.d0 + dx*(i-1)
				y = -(Ly + dy)/2.d0 + dy*(j-1)
				z = -dz/2.d0 + dz*(k-1)

				! Volumen fraction
				! --------------------------------------------

				! Sessile droplet (Spherical cap)
				Rs = (Rdrop**2.d0+Hdrop**2.d0)/Hdrop/2.d0
				delr = 2.d0*dsqrt(2.d0)*epn
				rp = dsqrt(x**2.d0 + y**2.d0 + (z+Rs-Hdrop)**2.d0)

				! Spherical droplet (Full sphere)
				!Rs = Rdrop
				!delr = Hdrop/30.d0
				!rp = dsqrt(x**2.d0 + y**2.d0 + (z-2.d0*Rdrop)**2.d0)

				fi(i,j,k) 	 = 0.5d0*(1.d0+dtanh((Rs-rp)/delr))
				fiold(i,j,k) 	 = fi(i,j,k)
				ConvPrevC(i,j,k) = 0.d0
				
				! Liquid layer at z=0.3
				!fi(i,j,k) 	 = 0.5d0*(1.d0+dtanh((0.3d0-z)/(delr)))
				!fiold(i,j,k) 	 = fi(i,j,k)
				
				! Temperature
				! --------------------------------------------
				T(i,j,k) = 0.d0*fi(i,j,k) + (1.d0-fi(i,j,k))*(-z/Lz + 1.d0)
				Told(i,j,k) = T(i,j,k)
				ConvPrevT(i,j,k) = 0.d0
				
				! Species
				! --------------------------------------------

				!if((fi(i,j,k).gt.0.05d0).and.(fi(i,j,k).lt.0.95d0))then

 					p1 = psat(psA,psB,psC,T(i,j,k))
					SpMF(i,j,k) = (p1/(p1+(1.d0/Pabs-p1)/rMW))*(4.d0*fi(i,j,k)*(1.d0-fi(i,j,k)))
					SpMF(i,j,k) = 0.d0
				!else

					!SpMF(i,j,k) = 0.d0

				!endif
				SpMFold(i,j,k) = SpMF(i,j,k)
				ConvPrevS(i,j,k) = 0.d0

				! Pressure
				! --------------------------------------------
				!Hydrostatic pressure
				!p(i,j,k)= rho(rGd,rOd,fi(i,j,k),SpMF(i,j,k))*(Lz-z)/Fr
				!Surface tension
				p(i,j,k) = 0.d0

			enddo
		enddo
	enddo 

	! Velocity
	! --------------------------------------------
	u2(:,:,:)= 0.d0
	v2(:,:,:)= 0.d0
	w2(:,:,:)= 0.d0
	preU(:,:,:)= 0.d0
	preV(:,:,:)= 0.d0
	preW(:,:,:)= 0.d0
	

endif


! +==========================================================+
! | DATA LOAD - Simulation continued from last point         |	
! +==========================================================+

if(.not.(newrun))then

	write(6,101) "Loading data from previous run..."
	
	! Find latest bak file in results directory and extract the iteration number
 	call system('ls -t ./results/*.s.bak > ./results/list.tmp')
	open(unit=700,file='./results/list.tmp',status='unknown')
	read(700,'(a)') lastbak
	close(700)
	write(6,101) 'Resuming simulation from: '

	! Iteration number to feed it to the main program --> Integer
	read(lastbak(11:index(lastbak,'.s.bak')-1),*) prevIt 
	write(*,*) prevIt
	! Iteration number to form the filename to load the data --> String
	temp = lastbak(11:index(lastbak,'.s.bak')-1) 
	
	! Scalar variable - Important to keep consistent the loop and write order with the backup subroutine
	! ---------------------------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.s.bak'
	write(6,102) filename
	open(unit=700,file=filename,status='unknown')

	do k = 1,n+1
		do j = 1,m+1
			do i = 1,l+1

				read(700,*) 	p(i,j,k), & 
						fi(i,j,k), &
						fiold(i,j,k), &
						ConvPrevC(i,j,k), &
						SpMF(i,j,k), &
						SpMFold(i,j,k), &
						ConvPrevS(i,j,k), &
						T(i,j,k), &
						Told(i,j,k), &
						ConvPrevT(i,j,k)

			enddo
		enddo
	enddo 

	close(700)


	! Velocity u - Important to keep consistent the loop and write order with the backup subroutine
	! ---------------------------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.u.bak'
	write(6,102) filename
	open(unit=701,file=filename,status='unknown')

	do k = 1,n-1
		do j = 1,m-1
			do i = 1,l

				read(701,*) u2(i,j,k),preU(i,j,k)

			enddo
		enddo
	enddo 

	close(701)

	! Velocity v - Important to keep consistent the loop and write order with the backup subroutine
	! ---------------------------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.v.bak'
	write(6,102) filename
	open(unit=702,file=filename,status='unknown')

	do k = 1,n-1
		do j = 1,m
			do i = 1,l-1

				read(702,*) v2(i,j,k),preV(i,j,k)

			enddo
		enddo
	enddo 

	close(702)

 	! Velocity u - Important to keep consistent the loop and write order with the backup subroutine
	! ---------------------------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.w.bak'
	write(6,102) filename
	open(unit=703,file=filename,status='unknown')

	do k = 1,n
		do j = 1,m-1
			do i = 1,l-1

				read(703,*) w2(i,j,k),preW(i,j,k)

			enddo
		enddo
	enddo 

	close(703)

endif

100   format(/,a)      
101   format(4x,a)
102   format(8x,a)

end subroutine data_load

! ##########################################################################################
! ##########################################################################################

subroutine flow_output &
(l,m,n,dx,dy,dz,Lx,Ly,u,v,w,p,finew,Tnew,SpMFnew,Sm,fileid)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz,Lx,Ly
	double precision,	intent(in)	::	u(l  ,m-1,n-1)
	double precision,	intent(in)	::	v(l-1,m  ,n-1)
	double precision,	intent(in)	::	w(l-1,m-1,n  )

	double precision,	intent(in)	::	p      (l+1,m+1,n+1)
	double precision,	intent(in)	::	finew  (l+1,m+1,n+1)
	double precision,	intent(in)	::	Tnew   (l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFnew(l+1,m+1,n+1)
	double precision,	intent(in)	::	Sm     (l+1,m+1,n+1)
	
	integer,		intent(in)	::	fileid

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	character(len=20)			::	temp
	character(len=25)			::	filename
	integer 				::	i,j,k	
	double precision			::	x,y,z
	double precision			::	ucc,vcc,wcc


! +======================================+
! | CALCULATIONS			 |
! +======================================+

 	write(temp,*) fileid
 	!write(*,*) temp

 	temp = adjustl(temp)
 	!write(*,*) temp
	
	filename='results/' // trim(temp) // '.dat'
	write(6,'(/,4x,a,/,4x,a,/)')'Saving intermediate file:',filename
	
	open(unit=9,file=filename,status='unknown')
	write(9,'(a)')'TITLE = "3D Droplet" '
	write(9,'(a)')'VARIABLES="x" "y" "z" "u" "v" "w" "fi" "p" "T" "mf" "Sm"'
	write(9,'(a,a,a,a,a,a,a,i7,a,i7,a,i7,a)')'ZONE T="Drop:',trim(temp),'",STRANDID=',trim(temp),',SOLUTIONTIME=',trim(temp),',I=',l-1,',J=',m-1,',K=',n-1,',F=POINT'



	! Variables stored at cells' centres=
	do k = 2,n
		
		z = -dz/2.d0 + dz*(k-1)

		do j = 2,m

			y = -(Ly + dy)/2.d0 + dy*(j-1)
		
			do i = 2,l

				x = -(Lx + dx)/2.d0 + dx*(i-1)
	
				! Region outside the physical domain (Ghost cells)	
				!if ((i.eq.1).and.(j.ne.1).and.(j.ne.(m+1)).and.(k.ne.1).and.(j.ne.(n+1))) then

				!	ucc = u(i,j-1,k-1)
				!	vcc = (v(i,j-1,k-1)+v(i,j,k-1))/2.d0
				!	wcc = (w(i,j-1,k-1)+w(i,j-1,k))/2.d0
				
				!elseif ((i.eq.(l+1)).and.(j.ne.1).and.(j.ne.(m+1)).and.(k.ne.1).and.(j.ne.(n+1))) then

				!	ucc = u(i+1,j-1,k-1)
				!	vcc = (v(i-2,j-1,k-1)+v(i-2,j,k-1))/2.d0
				!	wcc = (w(i-2,j-1,k-1)+w(i-2,j-1,k))/2.d0

				!elseif ((j.eq.1).and.(i.ne.1).and.(i.ne.(l+1)).and.(k.ne.1).and.(j.ne.(n+1))) then

				!	ucc = (u(i-1,j-1,k-1)+u(i  ,j-1,k-1))/2.d0
				!	vcc = (v(i-1,j-1,k-1)+v(i-1,j  ,k-1))/2.d0
				!	wcc = (w(i-1,j-1,k-1)+w(i-1,j-1,k  ))/2.d0

				!else ! Region within the physical domain	

					ucc = (u(i-1,j-1,k-1)+u(i  ,j-1,k-1))/2.d0
					vcc = (v(i-1,j-1,k-1)+v(i-1,j  ,k-1))/2.d0
					wcc = (w(i-1,j-1,k-1)+w(i-1,j-1,k  ))/2.d0

				!endif 

				write(9,'(11(ES18.6E3))')x,y,z,ucc,vcc,wcc,finew(i,j,k),p(i,j,k),Tnew(i,j,k),SpMFnew(i,j,k),Sm(i,j,k)
	
			enddo
		enddo
	enddo 


	close(9)
return
end subroutine flow_output

! ##########################################################################################
! ##########################################################################################

subroutine mesh_output &
(l,m,n,dx,dy,dz,Lx,Ly)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz

	double precision,	intent(in)	::	Lx,Ly

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	character(len=25)			::	filename
	integer 				::	i,j,k
	double precision			::	x,y,z


! +======================================+
! | CALCULATIONS			 |
! +======================================+

 	
	filename='results/XYZ.dat'
	write(6,'(/,4x,a,/,4x,a,/)')'Saving mesh coordiantes:',filename
	
	open(unit=9,file=filename,status='unknown')
	write(9,'(a)')'TITLE = "3D Droplet" '
	write(9,'(a)')'VARIABLES="X" "Y" "Z" '
	write(9,'(a,i7,a,i7,a,i7,a)')'ZONE T="Drop", I=',l-1,',J=',m-1,',K=',n-1,',F=POINT'

	! Coordiantes at cells' centres
	do k = 2,n
		
		z = -dz/2.d0 + dz*(k-1)
	
		do j = 2,m
			
			y = -(Ly + dy)/2.d0 + dy*(j-1)
	
			do i = 2,l
	
				x = -(Lx + dx)/2.d0 + dx*(i-1)
				
				write(9,'(3(ES18.6E3))')x,y,z
	
			enddo
		enddo
	enddo 

	close(9)
return
end subroutine mesh_output

! ##########################################################################################
! ##########################################################################################

subroutine backup &
(l,m,n,u2,v2,w2,preU,preV,preW,p,fi,fiold,ConvPrevC,SpMF,SpMFold,ConvPrevS,T,Told,ConvPrevT,fileid)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

 	double precision,	intent(in)	::	u2(l  ,m-1,n-1)
 	double precision,	intent(in)	::	v2(l-1,m  ,n-1)
 	double precision,	intent(in)	::	w2(l-1,m-1,n  )

 	double precision,	intent(in)	::	preU(l  ,m-1,n-1)
 	double precision,	intent(in)	::	preV(l-1,m  ,n-1)
 	double precision,	intent(in)	::	preW(l-1,m-1,n  )

 	double precision,	intent(in)	::	p        (l+1,m+1,n+1)
	double precision,	intent(in)	::	fi  	 (l+1,m+1,n+1)
	double precision,	intent(in)	::	fiold  	 (l+1,m+1,n+1)
	double precision,	intent(in)	::	ConvPrevC(l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMF 	 (l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFold  (l+1,m+1,n+1)
	double precision,	intent(in)	::	ConvPrevS(l+1,m+1,n+1)
	double precision,	intent(in)	::	T        (l+1,m+1,n+1)
	double precision,	intent(in)	::	Told     (l+1,m+1,n+1)
	double precision,	intent(in)	::	ConvPrevT(l+1,m+1,n+1)
	
	integer,		intent(in)	::	fileid

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	character(len=30)			::	temp
	character(len=60)			::	lastbak,temp2,temp3
	character(len=30)			::	filename
	integer 				::	i,j,k


! +======================================+
! | CALCULATIONS			 |
! +======================================+
	write(6,*) 'Saving new .bak files...'
	
	write(temp,*) fileid
 	!write(*,*) temp

 	temp = adjustl(temp)
 	!write(*,*) temp
	

	! Scalar variable - Important to keep consistent the loop and write order with the reading subroutine
	! ---------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.s.bak'
	write(6,*) filename
	open(unit=600,file=filename,status='unknown')

	do k = 1,n+1
		do j = 1,m+1
			do i = 1,l+1

				write(600,*) 	p(i,j,k), & 
						fi(i,j,k), &
						fiold(i,j,k), &
						ConvPrevC(i,j,k), &
						SpMF(i,j,k), &
						SpMFold(i,j,k), &
						ConvPrevS(i,j,k), &
						T(i,j,k), &
						Told(i,j,k), &
						ConvPrevT(i,j,k)

			enddo
		enddo
	enddo 

	close(600)


	! Velocity u - Important to keep consistent the loop and write order with the reading subroutine
	! ----------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.u.bak'
	write(6,*) filename
	open(unit=601,file=filename,status='unknown')

	do k = 1,n-1
		do j = 1,m-1
			do i = 1,l

				write(601,*) u2(i,j,k),preU(i,j,k)

			enddo
		enddo
	enddo 

	close(601)

	! Velocity v - Important to keep consistent the loop and write order with the reading subroutine
	! ----------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.v.bak'
	write(6,*) filename
	open(unit=602,file=filename,status='unknown')

	do k = 1,n-1
		do j = 1,m
			do i = 1,l-1

				write(602,*) v2(i,j,k),preV(i,j,k)

			enddo
		enddo
	enddo 

	close(602)

 	! Velocity u - Important to keep consistent the loop and write order with the reading subroutine
	! ----------------------------------------------------------------------------------
	filename='results/' // trim(temp) // '.w.bak'
	write(6,*) filename
	open(unit=603,file=filename,status='unknown')

	do k = 1,n
		do j = 1,m-1
			do i = 1,l-1

				write(603,*) w2(i,j,k),preW(i,j,k)

			enddo
		enddo
	enddo 

	close(603)


	! ---------------------------------------------------------------------------------
	! Delete previous .bak files if they exist 
	! Find oldest bak file in results directory and extract the iteration number
 	call system('ls -t ./results/*.s.bak | tail -1 > ./results/list.tmp')
	open(unit=700,file='./results/list.tmp',status='unknown')
	read(700,'(a)') lastbak
	close(700)
	
	! Iteration number to form the filename to load the data --> String
	temp2 = lastbak(11:index(lastbak,'.s.bak')-1) 

! Only delete file if those files are not the ones just created
! if(temp.ne.temp2)then
! 	write(6,*) 'Deleting previous .bak files...'
! 	temp3 = 'rm ./results/' // trim(temp2) // '.s.bak'
! 	write(6,*) 'results/' // trim(temp2) // '.s.bak'
! 	call system(temp3)
! 	temp3 = 'rm ./results/' // trim(temp2) // '.u.bak'
! 	write(6,*) 'results/' // trim(temp2) // '.u.bak'
! 	call system(temp3)
! 	temp3 = 'rm ./results/' // trim(temp2) // '.v.bak'
! 	write(6,*) 'results/' // trim(temp2) // '.v.bak'
! 	call system(temp3)
! 	temp3 = 'rm ./results/' // trim(temp2) // '.w.bak'
! 	write(6,*) 'results/' // trim(temp2) // '.w.bak'
! 	call system(temp3)
! endif 

return
end subroutine backup

! ##########################################################################################
! ##########################################################################################

subroutine sliceYZ_output_GC &
(l,m,n,dy,dz,Ly,u,v,w,p,finew,Tnew,SpMFnew,Sm,fileid)

! +======================================+
! | DEFINITION				 |
! +======================================+

! This subroutine INCLUDES the values at the ghost cells

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dy,dz,Ly
	double precision,	intent(in)	::	u(l  ,m-1,n-1)
	double precision,	intent(in)	::	v(l-1,m  ,n-1)
	double precision,	intent(in)	::	w(l-1,m-1,n  )

	double precision,	intent(in)	::	p      (l+1,m+1,n+1)
	double precision,	intent(in)	::	finew  (l+1,m+1,n+1)
	double precision,	intent(in)	::	Tnew   (l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFnew(l+1,m+1,n+1)
	double precision,	intent(in)	::	Sm     (l+1,m+1,n+1)
	
	integer,		intent(in)	::	fileid

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	character(len=20)			::	temp
	character(len=45)			::	filename
	integer 				::	i,j,k,i_secYZ
	double precision			::	ucc,vcc,wcc,y,z,SumSm
	logical 				:: 	dir_e

! +======================================+
! | CALCULATIONS			 |
! +======================================+

	if(mod(l,2).ne.0)then
		write(6,*) "Error: The number of cells within the physical domain is not an odd number in the x direction."
		STOP
	endif
	

	! Check whether the output directory exists or not
	INQUIRE(DIRECTORY='results/slices', EXIST=dir_e)

	if(.not.(dir_e))then

		call system('mkdir results/slices')
	
	endif

	i_secYZ = l/2+1
	
 	write(temp,*) fileid
 	!write(*,*) temp

 	temp = adjustl(temp)
 	!write(*,*) temp
	
	filename='results/slices/slice_YZ_' // trim(temp) // '.dat'
	write(6,'(/,4x,a,/,4x,a,/)')'Saving intermediate file:',filename
	
	open(unit=9,file=filename,status='unknown')
	write(9,'(a)')'TITLE = "2D sliceYZ" '
	write(9,'(a)')'VARIABLES="Y" "Z" "u" "v" "w" "fi" "p" "T" "mf" "Sm"'
	write(9,'(a,a,a,a,a,a,a,i7,a,i7,a,i7,a)')'ZONE T="sliceYZ:',trim(temp),'",STRANDID=',trim(temp),',SOLUTIONTIME=',trim(temp),',J=',m+1,',K=',n+1,',F=POINT'

	i = i_secYZ

	! Variables stored at cells' centres=
	do k = 1,n+1

		z = -dz/2.d0 + dz*(k-1)
	
		do j = 1,m+1
	
			y = -(Ly + dy)/2.d0 + dy*(j-1)
			
			if((k.eq.1).or.(k.eq.n+1).or.(j.eq.1).or.(j.eq.m+1))then
				ucc = 0.d0
				vcc = 0.d0
				wcc = 0.d0
			else
				ucc = (u(i-1,j-1,k-1)+u(i  ,j-1,k-1))/2.d0
				vcc = (v(i-1,j-1,k-1)+v(i-1,j  ,k-1))/2.d0
				wcc = (w(i-1,j-1,k-1)+w(i-1,j-1,k  ))/2.d0
			endif

				
			write(9,'(10(ES18.6E3))') y,z,ucc,vcc,wcc, &
 						finew	(i,j,k), &
 						p   	(i,j,k), &
 						Tnew	(i,j,k), &
 						SpMFnew	(i,j,k), &
 						Sm  	(i,j,k)
				
		enddo
	enddo 


	close(9)


	!Evaporation profile
	filename='results/slices/evap_YZ_' // trim(temp) // '.dat'
	write(6,'(/,4x,a,/,4x,a,/)')'Saving intermediate file:',filename
	
	open(unit=9,file=filename,status='unknown')
	write(9,'(a)')'y SumSm'
	
	! Variables stored at cells' centres=
	do j = 2,m

		y = -(Ly + dy)/2.d0 + dy*(j-1)
		SumSm = 0.d0

		do k = 2,n
			SumSm = SumSm + Sm(i_secYZ,j,k)
		enddo

		write(9,'(2(ES18.6E3))') y,SumSm

	enddo 

	close(9)

return
end subroutine sliceYZ_output_GC

! ##########################################################################################
! ##########################################################################################

subroutine sliceYZ_output2 &
(l,m,n,dy,dz,Ly,u,v,w,p,finew,Tnew,SpMFnew,Sm,fileid)

! +======================================+
! | DEFINITION				 |
! +======================================+

! This subroutine EXCLUDES the values at the ghost cells

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dy,dz,Ly
	double precision,	intent(in)	::	u(l  ,m-1,n-1)
	double precision,	intent(in)	::	v(l-1,m  ,n-1)
	double precision,	intent(in)	::	w(l-1,m-1,n  )

	double precision,	intent(in)	::	p      (l+1,m+1,n+1)
	double precision,	intent(in)	::	finew  (l+1,m+1,n+1)
	double precision,	intent(in)	::	Tnew   (l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFnew(l+1,m+1,n+1)
	double precision,	intent(in)	::	Sm     (l+1,m+1,n+1)
	
	integer,		intent(in)	::	fileid

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	character(len=20)			::	temp
	character(len=45)			::	filename
	integer 				::	i,j,k,i_secYZ
	double precision			::	ucc,vcc,wcc,y,z
	logical 				:: 	dir_e

! +======================================+
! | CALCULATIONS			 |
! +======================================+

	if(mod(l,2).ne.0)then
		write(6,*) "Error: The number of cells within the physical domain is not an odd number in the x direction."
		STOP
	endif
	

	! Check whether the output directory exists or not
	INQUIRE(DIRECTORY='results/slices', EXIST=dir_e)

	if(.not.(dir_e))then

		call system('mkdir results/slices')
	
	endif

	i_secYZ = l/2+1
	
 	write(temp,*) fileid
 	!write(*,*) temp

 	temp = adjustl(temp)
 	!write(*,*) temp
	
	filename='results/slices/slice_YZ_' // trim(temp) // '.dat'
	write(6,'(/,4x,a,/,4x,a,/)')'Saving intermediate file:',filename
	
	open(unit=9,file=filename,status='unknown')
	write(9,'(a)')'TITLE = "2D sliceYZ" '
	write(9,'(a)')'VARIABLES="Y" "Z" "u" "v" "w" "fi" "p" "T" "mf" "Sm"'
	write(9,'(a,a,a,a,a,a,a,i7,a,i7,a,i7,a)')'ZONE T="sliceYZ:',trim(temp),'",STRANDID=',trim(temp),',SOLUTIONTIME=',trim(temp),',J=',m-1,',K=',n-1,',F=POINT'

	! Variables stored at cells' centres=
	do k = 2,n

		z = -dz/2.d0 + dz*(k-1)
	
		do j = 2,m
	
			y = -(Ly + dy)/2.d0 + dy*(j-1)
			
			i = i_secYZ

			ucc = (u(i-1,j-1,k-1)+u(i  ,j-1,k-1))/2.d0
			vcc = (v(i-1,j-1,k-1)+v(i-1,j  ,k-1))/2.d0
			wcc = (w(i-1,j-1,k-1)+w(i-1,j-1,k  ))/2.d0


				
			write(9,'(10(ES18.6E3))') y,z,ucc,vcc,wcc, &
 						finew	(i,j,k), &
 						p   	(i,j,k), &
 						Tnew	(i,j,k), &
 						SpMFnew	(i,j,k), &
 						Sm  	(i,j,k)
				
		enddo
	enddo 


	close(9)
return
end subroutine sliceYZ_output2

! ##########################################################################################
! ##########################################################################################

subroutine drop_parameters(l,m,n,dx,dy,dz,dt,Lx,Ly,fi,Sm,iteration)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n,iteration

	double precision,	intent(in)	::	dx,dy,dz,dt

	double precision,	intent(in)	::	Lx,Ly
	double precision,	intent(in)	::	fi(l+1,m+1,n+1)
	double precision,	intent(in)	::	Sm(l+1,m+1,n+1)

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	character(len=40)			::	filename
	integer 				::	i,j,k,jxz,iyz
	double precision			::	x,y,z,time
	double precision			::	cx,cy,cz,ds,PI

	double precision			::	citaxz(l+1,n+1)
	double precision			::	citayz(m+1,n+1)

	double precision			::	citamax_xz,citamax_yz,ctemp

	integer 				::	XZcount,YZcount,Hcount
	double precision			::	XZradius,YZradius,HDrop,totSm,totVol

! +======================================+
! | CALCULATIONS			 |
! +======================================+

	PI = 4.d0*atan(1.d0)

	time = iteration*dt

! -----------------------------------------------------------------------
! ---- Contact angle & Base radius --------------------------------------
! -----------------------------------------------------------------------

! ---- XZ section --------------------------------------------------------
! Contact angle
  	citamax_xz = 0.d0
	
	jxz = m/2+1

do k=2,n
	do i=2,l
		cx=(fi(i+1,jxz,k  )-fi(i-1,jxz,k  ))/(2.d0*dx)
		cz=(fi(i  ,jxz,k+1)-fi(i  ,jxz,k-1))/(2.d0*dz)
		if(cx*cx>1e-8)then
			citaxz(i,k) = 90.d0+atan(cz/dsqrt(cx*cx))*180.d0/PI
		else
			citaxz(i,k) = 0.d0
		endif
	enddo
enddo

do k=2,n
	do i=2,l
		if((fi(i,jxz,k)<=0.5d0.and.fi(i+1,jxz,k)>0.5d0).or. &
		   (fi(i,jxz,k)>=0.5d0.and.fi(i+1,jxz,k)<0.5d0))then

			ds=(0.5d0-fi(i,jxz,k))/(fi(i+1,jxz,k)-fi(i,jxz,k))
			ctemp=(1.d0-ds)*citaxz(i,k)+ds*citaxz(i+1,k)
			if(citamax_xz<ctemp)citamax_xz=ctemp

		endif  

		if((fi(i,jxz,k)<=0.5d0.and.fi(i,jxz,k+1)>0.5d0).or. &
		   (fi(i,jxz,k)>=0.5d0.and.fi(i,jxz,k+1)<0.5d0))then
			
			ds=(0.5d0-fi(i,jxz,k))/(fi(i,jxz,k+1)-fi(i,jxz,k))	
			ctemp=(1.d0-ds)*citaxz(i,k)+ds*citaxz(i,k+1)
			if(citamax_xz<ctemp)citamax_xz=ctemp

		endif
                         
	enddo
enddo


! Base radius
	XZcount=0
	XZradius=0.d0

do i=2,l-1
	if((fi(i,jxz,2)<=0.5d0.and.fi(i+1,jxz,2)>0.5d0).or. &
	   (fi(i,jxz,2)>=0.5d0.and.fi(i+1,jxz,2)<0.5d0))then
		XZcount=XZcount+1
		ds=dx*(0.5d0-fi(i,jxz,2))/(fi(i+1,jxz,2)-fi(i,jxz,2))
		x = -(Lx + dx)/2.d0 + dx*(i-1) + ds
		XZradius = XZradius+dsqrt(x*x)
	endif
enddo

XZradius = XZradius/float(XZcount)

!---- YZ section --------------------------------------------------------
! Contact angle
	iyz = l/2+1
	citamax_yz = 0.d0
	

do k=2,n
	do j=2,m
		cy=(fi(iyz,j+1,k  )-fi(iyz,j-1,k  ))/(2.d0*dy)
		cz=(fi(iyz,j  ,k+1)-fi(iyz,j  ,k-1))/(2.d0*dz)
		if(cy*cy>1e-8)then
			citayz(j,k) = 90.d0+atan(cz/dsqrt(cy*cy))*180.d0/PI
		else
			citayz(j,k) = 0.d0
		endif
	enddo
enddo

do k=2,n
	do j=2,m
		if((fi(iyz,j,k)<=0.5d0.and.fi(iyz,j+1,k)>0.5d0).or. &
		   (fi(iyz,j,k)>=0.5d0.and.fi(iyz,j+1,k)<0.5d0))then

			ds=(0.5d0-fi(iyz,j,k))/(fi(iyz,j+1,k)-fi(iyz,j,k))
			ctemp=(1.d0-ds)*citayz(j,k)+ds*citayz(j+1,k)
			if(citamax_yz<ctemp)citamax_yz=ctemp

		endif  

		if((fi(iyz,j,k)<=0.5d0.and.fi(iyz,j,k+1)>0.5d0).or. &
		   (fi(iyz,j,k)>=0.5d0.and.fi(iyz,j,k+1)<0.5d0))then
			
			ds=(0.5d0-fi(iyz,j,k))/(fi(iyz,j,k+1)-fi(iyz,j,k))	
			ctemp=(1.d0-ds)*citayz(j,k)+ds*citayz(j,k+1)
			if(citamax_yz<ctemp)citamax_yz=ctemp

		endif
                         
	enddo
enddo


! Base radius
	YZcount=0
	YZradius=0.d0

do j=2,m-1
	if((fi(iyz,j,2)<=0.5d0.and.fi(iyz,j+1,2)>0.5d0).or. &
	   (fi(iyz,j,2)>=0.5d0.and.fi(iyz,j+1,2)<0.5d0))then
		YZcount=YZcount+1
		ds=dy*(0.5d0-fi(iyz,j,2))/(fi(iyz,j+1,2)-fi(iyz,j,2))
		y = -(Ly + dy)/2.d0 + dy*(j-1) + ds
		YZradius = YZradius+dsqrt(y*y)
	endif
enddo

YZradius = YZradius/float(YZcount)


! -----------------------------------------------------------------------
! --- Droplet height ----------------------------------------------------
! -----------------------------------------------------------------------
Hcount=0
HDrop=0.d0

do k=2,n-1
	if((fi(iyz,jxz,k)>=0.5d0).and.(fi(iyz,jxz,k+1)<0.5d0))then
		Hcount=Hcount+1
		ds=dz*(0.5d0-fi(iyz,jxz,k))/(fi(iyz,jxz,k+1)-fi(iyz,jxz,k))
		z = -dz/2.d0 + dz*(k-1) + ds
		HDrop = HDrop+dsqrt(z*z)
	endif
enddo

HDrop = HDrop/float(Hcount)

! -----------------------------------------------------------------------
! --- Total Sm & Volume of liquid ---------------------------------------
! -----------------------------------------------------------------------
totSm  = 0.d0
totVol = 0.d0

!$OMP PARALLEL DO REDUCTION(+:totSm,totVol) DEFAULT(SHARED) PRIVATE(i,j,k)	
do i = 2,l
	do j = 2,m
		do k = 2,n

			totSm  = totSm  + Sm(i,j,k)
			totVol = totVol + fi(i,j,k)*dx*dy*dz
		
		enddo
	enddo
enddo 
!$OMP END PARALLEL DO


	filename='results/droplet_evolution.dat'
	open(unit=501,file=filename,status='unknown',position='append')

	if(iteration.eq.0)then

		write(501,*)"time XZradius YZradius citamax_xz citamax_yz HDrop totVol totSm"

	endif
	write(501,'(8(ES18.6E3))')time,XZradius,YZradius,citamax_xz,citamax_yz,HDrop,totVol,totSm
	close(501)
	

return

end subroutine drop_parameters
