subroutine Temperature &
(l,m,n,dx,dy,dz,dt,Re,Pr,Ja,rGd,rGcp,rGk,rOd,rOcp,rOk,u,v,w,Sm,fi,finew,SpMF,SpMFnew,T,Told,num_threads,ConvPrevT,res,iteration,Tnew)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz,dt
	double precision,	intent(in)	::	Re,Pr,Ja
	double precision,	intent(in)	::	rGd,rGcp,rGk
	double precision,	intent(in)	::	rOd,rOcp,rOk

	double precision,	intent(in)	::	u(l  ,m-1,n-1)
	double precision,	intent(in)	::	v(l-1,m  ,n-1)
	double precision,	intent(in)	::	w(l-1,m-1,n  )

	double precision,	intent(in)	::	Sm(l+1,m+1,n+1)

	double precision,	intent(in)	::	fi   (l+1,m+1,n+1)
	double precision,	intent(in)	::	finew(l+1,m+1,n+1)

	double precision,	intent(in)	::	SpMF   (l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFnew(l+1,m+1,n+1)

	double precision,	intent(in)	::	T   (l+1,m+1,n+1)
	double precision,	intent(in)	::	Told(l+1,m+1,n+1)

	integer,		intent(in)	::	num_threads

	double precision,	intent(inout)	::	ConvPrevT(l+1,m+1,n+1)
	double precision,	intent(inout)	::	Tnew(l+1,m+1,n+1)
	double precision,	intent(out)	::	res
	integer,		intent(out)	::	iteration

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	integer 				::	i,j,k

	double precision, 	allocatable	::	source(:,:,:)
	double precision, 	allocatable	::	d2T(:,:,:)

	double precision, 	allocatable	::	rho(:,:,:)
	double precision, 	allocatable	::	hcap(:,:,:)
	double precision, 	allocatable	::	cond(:,:,:)

	double precision 			::	temp1,temp2,temp3
	double precision 			::	a1,a2,a3,a4,a5,a6
	double precision 			::	ctemp,ytemp,rhob,cpb,kb

	double precision 			::	some
	double precision 			::	diagonal
	double precision,	parameter	::	alfa1 = 1.1d0
	double precision, 	parameter 	:: 	tol = 5.d-9
	integer, 		parameter 	::	iterNum = 18

	integer 				::	tid
	integer 				::	OMP_GET_THREAD_NUM
	integer 				::	OMP_GET_NUM_THREADS

        double precision 			:: 	res_vec(0:num_threads-1)

	integer					::	alloc_stat


! +======================================+
! | CALCULATIONS			 |
! +======================================+
	

! ************************************************************************
! ALLOCATE ALL ARRAYS
! ************************************************************************

	allocate(source(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(d2T(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	allocate(rho (1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(hcap(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(cond(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

! ************************************************************************
! ************************************************************************

	!$OMP PARALLEL WORKSHARE
	source 	= 0.d0
	d2T 	= 0.d0
	rho 	= 0.d0
	hcap 	= 0.d0
	cond	= 0.d0
	!$OMP END PARALLEL WORKSHARE	

		
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,a1,a2,a3,a4,a5,a6,temp1,temp2,temp3,ctemp,ytemp,rhob,cpb,kb)
do i=2,l
	do j=2,m
		do k=2,n
			ctemp = (fi(i,j,k)  +finew(i,j,k)  )/2.d0
			ytemp = (SpMF(i,j,k)+SpMFnew(i,j,k))/2.d0

			rhob = 1.d0/(rGd*(1.d0-ytemp)+rOd*ytemp)
			cpb  = (1.d0-ytemp)/rGcp + ytemp/rOcp
			kb   = (1.d0-ytemp)/rGk  + ytemp/rOk
			
			rho (i,j,k) = ctemp + rhob*(1.d0-ctemp)
			cond(i,j,k) = ctemp + kb*(1.d0-ctemp)
			hcap(i,j,k) = (ctemp+rhob*cpb*(1.d0-ctemp))/(ctemp+rhob*(1.d0-ctemp))

			temp1 = Sm(i,j,k)/(Ja*rho(i,j,k)*hcap(i,j,k))

			a1 = (T(i-1,j,k) + T(i,j,k))/2.d0
			a2 = (T(i+1,j,k) + T(i,j,k))/2.d0

			a3 = (T(i,j-1,k) + T(i,j,k))/2.d0
			a4 = (T(i,j+1,k) + T(i,j,k))/2.d0

			a5 = (T(i,j,k-1) + T(i,j,k))/2.d0
			a6 = (T(i,j,k+1) + T(i,j,k))/2.d0

			
			temp2 = &
				 ( a2*u(i  ,j-1,k-1) - a1*u(i-1,j-1,k-1) )/dx &
				+( a4*v(i-1,j  ,k-1) - a3*v(i-1,j-1,k-1) )/dy &
				+( a6*w(i-1,j-1,k  ) - a5*w(i-1,j-1,k-1) )/dz 

			temp3 = &
				 ( ( T(i+1,j  ,k  ) - T(i  ,j  ,k  ) ) &
				  -( T(i  ,j  ,k  ) - T(i-1,j  ,k  ) ) )/dx/dx &
				+( ( T(i  ,j+1,k  ) - T(i  ,j  ,k  ) ) &
				  -( T(i  ,j  ,k  ) - T(i  ,j-1,k  ) ) )/dy/dy &
				+( ( T(i  ,j  ,k+1) - T(i  ,j  ,k  ) ) &
				  -( T(i  ,j  ,k  ) - T(i  ,j  ,k-1) ) )/dz/dz


			source(i,j,k)= &
				temp3*cond(i,j,k)/(2.d0*Re*Pr*rho(i,j,k)*hcap(i,j,k)) &
				-1.5d0*temp2 - 0.5d0*ConvPrevT(i,j,k) - temp1

			ConvPrevT(i,j,k) = -temp2
		
		enddo
	enddo
enddo
!$OMP END PARALLEL DO


a1 = 1.d0/(dx**2.d0) + 1.d0/(dy**2.d0) + 1.d0/(dz**2.d0)

do iteration=1, iterNum
	res =0.d0

	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
	do i = 2,l
		do j = 2,m
			do k = 2,n
	
				d2T(i,j,k) = &
				 ( ( Tnew(i+1,j  ,k  ) - Tnew(i  ,j  ,k  ) ) &
				  -( Tnew(i  ,j  ,k  ) - Tnew(i-1,j  ,k  ) ) )/dx/dx &
				+( ( Tnew(i  ,j+1,k  ) - Tnew(i  ,j  ,k  ) ) &
				  -( Tnew(i  ,j  ,k  ) - Tnew(i  ,j-1,k  ) ) )/dy/dy &
				+( ( Tnew(i  ,j  ,k+1) - Tnew(i  ,j  ,k  ) ) &
				  -( Tnew(i  ,j  ,k  ) - Tnew(i  ,j  ,k-1) ) )/dz/dz
						
				
			enddo
		enddo
	enddo 
	!$OMP END PARALLEL DO


	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,diagonal,some,res,tid,temp1)
	tid = OMP_GET_THREAD_NUM()
	res = 0.d0

	!$OMP DO
	do i=2,l
		do j=2,m
			do k=2,n
				
				temp1 = cond(i,j,k)/(rho(i,j,k)*hcap(i,j,k))

				diagonal=1.5d0 + dt*temp1*a1/Re/Pr

				some =   dt*temp1*d2T(i,j,k)/2.d0/Re/Pr + dt*source(i,j,k) + 2.d0*T(i,j,k) - 0.5d0*Told(i,j,k) - 1.5d0*Tnew(i,j,k)
				
				temp1 = Tnew(i,j,k)
				
				Tnew(i,j,k) = Tnew(i,j,k) + alfa1*some/diagonal
				
				if(Tnew(i,j,k).lt.0.d0)Tnew(i,j,k)=0.d0
				if(Tnew(i,j,k).gt.1.d0)Tnew(i,j,k)=1.d0

				! res=dmax1(res,dabs(some)/dt)
				res=dmax1(res,dabs(Tnew(i,j,k)-temp1)) 
				
			enddo
		enddo
	enddo
	!$OMP END DO
	
	res_vec(tid)=res
	!$OMP END PARALLEL

	
	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(j,k)
	do j=2,m
		do k=2,n
			!Non-periodic in x
			Tnew(1  ,j,k)=Tnew(2,j,k)
			Tnew(l+1,j,k)=Tnew(l,j,k)
		enddo
	enddo
	!$OMP END PARALLEL DO



	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,k)
	do i=2,l
		do k=2,n
			!Non-periodic in y
			Tnew(i,1  ,k)=Tnew(i,2,k)
			Tnew(i,m+1,k)=Tnew(i,m,k)
		enddo
	enddo
	!$OMP END PARALLEL DO

	
	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j)
	do i=2,l
		do j=2,m
			!Non-periodic in z
			Tnew(i,j,1  )=1.d0
			Tnew(i,j,n+1)=0.d0
		enddo
	enddo
	!$OMP END PARALLEL DO


	res=0.d0
	do tid=0,num_threads-1
	
		if(res_vec(tid).gt.res)then
			res=res_vec(tid)
		endif

	enddo
	
	if(res.lt.tol)go to 1
 	   
enddo

		

1	continue

! write(6,*) 'Iterations required to converge (T) = ',iteration


! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************
 
	deallocate(source,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(d2T,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	deallocate(rho ,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(hcap,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(cond,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

! ************************************************************************
! ************************************************************************

return
end subroutine Temperature

! #########################################################################
! #########################################################################

subroutine totenergy(iteration,dt,l,m,n,dx,dy,dz,rGd,rGcp,rOd,rOcp,T,fi,SpMF)
      
	implicit none

	integer,		intent(in)	::	l,m,n,iteration
	double precision,	intent(in)	::	dt,dx,dy,dz
	double precision,	intent(in)	::	rGd,rGcp,rOd,rOcp
	double precision,	intent(in)	::	T(l+1,m+1,n+1)
	double precision,	intent(in)	::	fi(l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMF(l+1,m+1,n+1)
		
!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision			::	totene,time,rhoB,cpB,rho,cp
	integer					::	i,j,k
	character(len=50)			::	filename

totene = 0.d0
time = iteration*dt

!$OMP PARALLEL DO REDUCTION(+:totene) DEFAULT(SHARED) PRIVATE(i,j,k,rhoB,cpB,rho,cp)	
do i = 2,l
	do j = 2,m
		do k = 2,n

			rhoB = 1.d0/(rGd*(1.d0-SpMF(i,j,k))+rOd*SpMF(i,j,k))
			rho  = fi(i,j,k)+rhoB*(1.d0-fi(i,j,k))

			cpB = (1.d0-SpMF(i,j,k))/rGcp + SpMF(i,j,k)/rOcp
			cp  = (fi(i,j,k)+rhoB*cpB*(1.d0-fi(i,j,k)))/(fi(i,j,k)+rhoB*(1.d0-fi(i,j,k)))

			!totene=totene+rho*cp*T(i,j,k)*dx*dy*dz
			totene=totene+T(i,j,k)*dx*dy*dz
					
		enddo
	enddo
enddo 
!$OMP END PARALLEL DO


	filename='results/inst_tot_energy.dat'
	open(unit=501,file=filename,status='unknown',position='append')
	write(501,'(2(ES20.8E3))')time,totene
	close(501)
	
return
end subroutine totenergy

! #########################################################################
! #########################################################################

subroutine UpdateT (l,m,n,Tnew,T,Told)

	implicit none

	integer,		intent(in)	::	l,m,n
	double precision, 	intent(in)	::	Tnew(l+1,m+1,n+1)
	double precision, 	intent(inout)	::	T   (l+1,m+1,n+1)
	double precision, 	intent(out)	::	Told(l+1,m+1,n+1)
	integer					::	i,j,k
	
!$OMP PARALLEL DO SCHEDULE(STATIC)  DEFAULT(SHARED) PRIVATE(i,j,k)
do i = 1,l+1
	do j = 1,m+1
		do k = 1,n+1
	
			Told(i,j,k)= T(i,j,k)
			T(i,j,k)= Tnew(i,j,k)

		enddo
	enddo
enddo 
!$OMP END PARALLEL DO

return
end subroutine UpdateT

