#!/bin/sh

# Grid Engine options
#$ -N wdt40_10
#$ -cwd
#$ -R y
#$ -pe OpenMP 6
#$ -l h_rt=48:00:00

#$ -M p.saenz@ed.ac.uk
#$ -m bes

ulimit -s unlimited

# Initialise environment module
. /etc/profile.d/modules.sh

# Use Intel compilers (icc,ifort)
module load intel

# Set number of threads
export OMP_NUM_THREADS=$NSLOTS
#echo OMP_NUM_THREADS=$OMP_NUM_THREADS
				
# Run OpenMP program
./droplet.exe
