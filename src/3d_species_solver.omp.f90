
subroutine Species &
(l,m,n,dx,dy,dz,dt,Re,Sc,rGd,rOd,u,v,w,Sm,fi,SpMF,SpMFold,filimmin,filimmax,num_threads,ConvPrevS,res,iteration,SpMFnew)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz,dt
	double precision,	intent(in)	::	Re
	double precision,	intent(in)	::	Sc
	double precision,	intent(in)	::	rGd
	double precision,	intent(in)	::	rOd

	double precision,	intent(in)	::	u(l  ,m-1,n-1)
	double precision,	intent(in)	::	v(l-1,m  ,n-1)
	double precision,	intent(in)	::	w(l-1,m-1,n  )

	double precision,	intent(in)	::	Sm(l+1,m+1,n+1)

	double precision,	intent(in)	::	fi(l+1,m+1,n+1)

	double precision,	intent(in)	::	SpMF   (l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFold(l+1,m+1,n+1)
	double precision,	intent(in)	::	filimmin 
	double precision,	intent(in)	::	filimmax 

	integer,		intent(in)	::	num_threads

	double precision,	intent(inout)	::	ConvPrevS(l+1,m+1,n+1)
	double precision,	intent(inout)	::	SpMFnew(l+1,m+1,n+1)
	double precision,	intent(out)	::	res
	integer,		intent(out)	::	iteration

	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	integer 				::	i,j,k

	double precision, 	allocatable	::	source(:,:,:)
	double precision, 	allocatable	::	d2Y(:,:,:)

	double precision 			::	temp1,temp2,temp3
	double precision 			::	a1,a2,a3,a4,a5,a6
	double precision 			::	fx1,fx2,fy1,fy2,fz1,fz2

	double precision 			::	some
	double precision 			::	diagonal
	double precision,	parameter	::	etiny = 1.d-6
	double precision,	parameter	::	alfa1 = 1.1d0
	double precision, 	parameter 	:: 	tol = 5.d-9
	integer, 		parameter 	::	iterNum = 18

	integer 				::	tid
	integer 				::	OMP_GET_THREAD_NUM
	integer 				::	OMP_GET_NUM_THREADS

        double precision 			:: 	res_vec(0:num_threads-1)

	integer					::	alloc_stat


! +======================================+
! | CALCULATIONS			 |
! +======================================+
	

! ************************************************************************
! ALLOCATE ALL ARRAYS
! ************************************************************************

	allocate(source(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(d2Y(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

! ************************************************************************
! ************************************************************************

	!$OMP PARALLEL WORKSHARE
	source 	= 0.d0
	d2Y 	= 0.d0
	!$OMP END PARALLEL WORKSHARE	

		
!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,a1,a2,a3,a4,a5,a6,temp1,temp2,temp3,fx1,fx2,fy1,fy2,fz1,fz2)
do i=2,l
	do j=2,m
		do k=2,n

			temp1 = Sm(i,j,k)*(rGd+(rOd-rGd)*SpMF(i,j,k))/(1.d0-fi(i,j,k)+etiny)

			call fluxes(l,m,n,i,j,k,fi,filimmin,filimmax,fx1,fx2,fy1,fy2,fz1,fz2)

			a1 = (SpMF(i-1,j,k) + SpMF(i,j,k))/2.d0
			a2 = (SpMF(i+1,j,k) + SpMF(i,j,k))/2.d0

			a3 = (SpMF(i,j-1,k) + SpMF(i,j,k))/2.d0
			a4 = (SpMF(i,j+1,k) + SpMF(i,j,k))/2.d0

			a5 = (SpMF(i,j,k-1) + SpMF(i,j,k))/2.d0
			a6 = (SpMF(i,j,k+1) + SpMF(i,j,k))/2.d0

			
			temp2 = &
				 ( fx2*a2*u(i  ,j-1,k-1) - fx1*a1*u(i-1,j-1,k-1) )/dx &
				+( fy2*a4*v(i-1,j  ,k-1) - fy1*a3*v(i-1,j-1,k-1) )/dy &
				+( fz2*a6*w(i-1,j-1,k  ) - fz1*a5*w(i-1,j-1,k-1) )/dz 

			temp3 = &
				 ( fx2*( SpMF(i+1,j  ,k  ) - SpMF(i  ,j  ,k  ) ) &
				  -fx1*( SpMF(i  ,j  ,k  ) - SpMF(i-1,j  ,k  ) ) )/dx/dx &
				+( fy2*( SpMF(i  ,j+1,k  ) - SpMF(i  ,j  ,k  ) ) &
				  -fy1*( SpMF(i  ,j  ,k  ) - SpMF(i  ,j-1,k  ) ) )/dy/dy &
				+( fz2*( SpMF(i  ,j  ,k+1) - SpMF(i  ,j  ,k  ) ) &
				  -fz1*( SpMF(i  ,j  ,k  ) - SpMF(i  ,j  ,k-1) ) )/dz/dz


			source(i,j,k)= temp3/(2.d0*Re*Sc)-1.5d0*temp2-0.5d0*ConvPrevS(i,j,k)+ temp1

			ConvPrevS(i,j,k) = -temp2
		
		enddo
	enddo
enddo
!$OMP END PARALLEL DO


a1 = 1.d0/(dx**2.d0) + 1.d0/(dy**2.d0) + 1.d0/(dz**2.d0)
diagonal=1.5d0 + dt*a1/Re/Sc

do iteration=1, iterNum
	res =0.d0

	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,fx1,fx2,fy1,fy2,fz1,fz2)
	do i = 2,l
		do j = 2,m
			do k = 2,n
	
				call fluxes(l,m,n,i,j,k,fi,filimmin,filimmax,fx1,fx2,fy1,fy2,fz1,fz2)

				d2Y(i,j,k) = &
				 ( fx2*( SpMFnew(i+1,j  ,k  ) - SpMFnew(i  ,j  ,k  ) ) &
				  -fx1*( SpMFnew(i  ,j  ,k  ) - SpMFnew(i-1,j  ,k  ) ) )/dx/dx &
				+( fy2*( SpMFnew(i  ,j+1,k  ) - SpMFnew(i  ,j  ,k  ) ) &
				  -fy1*( SpMFnew(i  ,j  ,k  ) - SpMFnew(i  ,j-1,k  ) ) )/dy/dy &
				+( fz2*( SpMFnew(i  ,j  ,k+1) - SpMFnew(i  ,j  ,k  ) ) &
				  -fz1*( SpMFnew(i  ,j  ,k  ) - SpMFnew(i  ,j  ,k-1) ) )/dz/dz
						
				
			enddo
		enddo
	enddo 
	!$OMP END PARALLEL DO


	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,some,res,tid,temp1)
	tid = OMP_GET_THREAD_NUM()
	res = 0.d0

	!$OMP DO
	do i=2,l
		do j=2,m
			do k=2,n

				some =   dt*d2Y(i,j,k)/2.d0/Re/Sc + dt*source(i,j,k) + 2.d0*SpMF(i,j,k) - 0.5d0*SpMFold(i,j,k) - 1.5d0*SpMFnew(i,j,k)
				
				temp1 = SpMFnew(i,j,k)
				
				if(fi(i,j,k).lt.filimmax)then
					SpMFnew(i,j,k) = SpMFnew(i,j,k) + alfa1*some/diagonal
				endif
			
				if(SpMFnew(i,j,k).lt.0.d0)SpMFnew(i,j,k)=0.d0
				if(SpMFnew(i,j,k).gt.1.d0)SpMFnew(i,j,k)=1.d0

				! res=dmax1(res,dabs(some)/dt)
				res=dmax1(res,dabs(SpMFnew(i,j,k)-temp1)) 
				
			enddo
		enddo
	enddo
	!$OMP END DO
	
	res_vec(tid)=res
	!$OMP END PARALLEL

	
	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(j,k)
	do j=2,m
		do k=2,n
			!Non-periodic in x
			SpMFnew(1  ,j,k)=SpMFnew(2,j,k)
			SpMFnew(l+1,j,k)=SpMFnew(l,j,k)
		enddo
	enddo
	!$OMP END PARALLEL DO



	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,k)
	do i=2,l
		do k=2,n
			!Non-periodic in y
			SpMFnew(i,1  ,k)=SpMFnew(i,2,k)
			SpMFnew(i,m+1,k)=SpMFnew(i,m,k)
		enddo
	enddo
	!$OMP END PARALLEL DO

	
	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j)
	do i=2,l
		do j=2,m
			!Non-periodic in z
			SpMFnew(i,j,1  )=SpMFnew(i,j,2)
			SpMFnew(i,j,n+1)=SpMFnew(i,j,n)
		enddo
	enddo
	!$OMP END PARALLEL DO


	res=0.d0
	do tid=0,num_threads-1
	
		if(res_vec(tid).gt.res)then
			res=res_vec(tid)
		endif

	enddo
	
	if(res.lt.tol)go to 1
 	   
enddo

		

1	continue

! write(6,*) 'Iterations required to converge (Sp) = ',iteration


! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************
 
	deallocate(source,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(d2Y,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

! ************************************************************************
! ************************************************************************

return
end subroutine Species

! #########################################################################
! #########################################################################

subroutine totvapmass(iteration,dt,l,m,n,fi,SpMF,rGd,rOd,dx,dy,dz)
      
	implicit none

	integer,		intent(in)	::	l,m,n,iteration
	double precision,	intent(in)	::	fi(l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMF(l+1,m+1,n+1)
	double precision,	intent(in)	::	dt,rGd,rOd,dx,dy,dz
	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision			::	totmass,time
	integer					::	i,j,k
	character(len=50)			::	filename

totmass = 0.d0
time = iteration*dt

!$OMP PARALLEL DO REDUCTION(+:totmass) DEFAULT(SHARED) PRIVATE(i,j,k)	
do i = 2,l
	do j = 2,m
		do k = 2,n
			
			totmass=totmass + (1.d0-fi(i,j,k))*SpMF(i,j,k)*dx*dy*dz
		
		enddo
	enddo
enddo 
!$OMP END PARALLEL DO


	filename='results/inst_tot_vap_mass.dat'
	open(unit=522,file=filename,status='unknown',position='append')
	write(522,'(2(ES20.8E3))')time,totmass
	close(522)
	
return
end subroutine totvapmass

! #########################################################################
! #########################################################################

subroutine fluxes &
(l,m,n,i,j,k,fi,fimin,fimax,fx1,fx2,fy1,fy2,fz1,fz2)

	implicit none

	integer,		intent(in)	::	l,m,n,i,j,k
	double precision,	intent(in)	::	fi(l+1,m+1,n+1)
	double precision,	intent(in)	::	fimin,fimax
	double precision,	intent(out)	::	fx1,fx2,fy1,fy2,fz1,fz2
	

	! This subroutine is very inefficient and can be improved once the code is working
	if(fi(i,j,k) .lt. fimin)then

		fx1=1.d0
		fx2=1.d0
		fy1=1.d0
		fy2=1.d0
		fz1=1.d0
		fz2=1.d0

	elseif(fi(i,j,k) .gt. fimax)then

		fx1=0.d0
		fx2=0.d0
		fy1=0.d0
		fy2=0.d0
		fz1=0.d0
		fz2=0.d0

	else

		if(fi(i-1,j,k) .gt. fimax) then
			fx1=0.d0
		else
			fx1=1.d0
		endif

		if(fi(i+1,j,k) .gt. fimax) then
			fx2=0.d0
		else
			fx2=1.d0
		endif

		if(fi(i,j-1,k) .gt. fimax) then
			fy1=0.d0
		else
			fy1=1.d0
		endif

		if(fi(i,j+1,k) .gt. fimax) then
			fy2=0.d0
		else
			fy2=1.d0
		endif

		if(fi(i,j,k-1) .gt. fimax) then
			fz1=0.d0
		else
			fz1=1.d0
		endif

		if(fi(i,j,k+1) .gt. fimax) then
			fz2=0.d0
		else
			fz2=1.d0
		endif

	endif

return
end subroutine fluxes

! #########################################################################
! #########################################################################

subroutine UpdateSpMF(l,m,n,SpMFnew,SpMF,SpMFold)

	implicit none

	integer,		intent(in)	::	l,m,n
	double precision,	intent(in)	::	SpMFnew(l+1,m+1,n+1)
	double precision,	intent(inout)	::	SpMF(l+1,m+1,n+1)
	double precision,	intent(out)	::	SpMFold(l+1,m+1,n+1)

	integer					::	i,j,k

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
do i = 1,l+1
	do j = 1,m+1
		do k = 1,n+1

			SpMFold(i,j,k)= SpMF(i,j,k)
			SpMF(i,j,k)= SpMFnew(i,j,k)

		enddo
	enddo
enddo 
!$OMP END PARALLEL DO


return
end subroutine UpdateSpMF
