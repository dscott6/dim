subroutine PhaseChange &
(l,m,n,dt,dz,rGd,rOd,rMW,Pabs,psA,psB,psC,filimmax,fi,SpMF,T,Sm)
		
! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dt,dz
	double precision,	intent(in)	::	rGd,rOd,rMW
	double precision,	intent(in)	::	Pabs,psA,psB,psC,filimmax

	double precision,	intent(in)	::	fi	(l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMF	(l+1,m+1,n+1)
	double precision,	intent(in)	::	T	(l+1,m+1,n+1)

	double precision,	intent(out)	::	Sm	(l+1,m+1,n+1)
	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	integer 				::	i,j,k
	double precision			::	temp1,temp2,temp3,p1,psat,Sm1

! +======================================+
! | CALCULATIONS			 |
! +======================================+
	

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,temp1,temp2,temp3,p1,Sm1)
do i = 2,l
	do j = 2,m
		do k = 2,n

			p1 = psat(psA,psB,psC,T(i,j,k))
	
			temp1 = p1/(p1+(1.d0/Pabs-p1)/rMW) - SpMF(i,j,k)
			
			temp2 = 1.d0/(rGd - rGd*SpMF(i,j,k) + rOd*SpMF(i,j,k))
			
			temp3 = 16.d0*(fi(i,j,k)**2.d0)*((1.d0-fi(i,j,k))**2.d0)
			
			Sm1 = (1.d0-fi(i,j,k))*temp1*temp2*temp3/dt


			Sm(i,j,k)=dmax1(Sm1,0.d0)

			if((fi(i,j,k).gt.filimmax).or.(fi(i,j,k).lt.0.01d0))then
				Sm(i,j,k) = 0.d0
			endif
		
		enddo
	enddo
enddo 
!$OMP END PARALLEL DO

return
end subroutine PhaseChange

! #########################################################################
! #########################################################################

function psat(psA,psB,psC,T)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************

!	Function arguments
!	-----------------------------------------------------------------------------+
	implicit none

	double precision			::	psat
	double precision			::	psA,psB,psC
	double precision			::	T

! +======================================+
! | CALCULATIONS			 |
! +======================================+
	
	psat = 10.d0**(psA-psB/(psC+T))
		
end function psat