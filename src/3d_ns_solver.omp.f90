subroutine NS_solver(l,m,n,dx,dy,dz,dt,epn,Re,We,Fr,Bod,rGd,rGv,rGb,rOd,rOv,rOb,u2,v2,w2,u3,v3,w3,preU,preV,preW,p,finew,fiold,SpMFnew,SpMFold,Tnew,Told,Sm,num_threads,nitU,nitP,resU,resP)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n,num_threads
	double precision,	intent(in)	::	dx,dy,dz,dt
	double precision,	intent(in)	::	epn,Re,We,Fr,Bod
	double precision,	intent(in)	::	rGd,rGv,rGb,rOd,rOv,rOb

	double precision,	intent(in)	::	u2(l  ,m-1,n-1)
	double precision,	intent(inout)	::	u3(l  ,m-1,n-1)
	double precision,	intent(in)	::	v2(l-1,m  ,n-1)
	double precision,	intent(inout)	::	v3(l-1,m  ,n-1)
	double precision,	intent(in)	::	w2(l-1,m-1,n  )
	double precision,	intent(inout)	::	w3(l-1,m-1,n  )

	double precision,	intent(inout)	::	preU(l  ,m-1,n-1)
	double precision,	intent(inout)	::	preV(l-1,m  ,n-1)
	double precision,	intent(inout)	::	preW(l-1,m-1,n  )

	double precision,	intent(inout)	::	p   (l+1,m+1,n+1)

	double precision,	intent(in)	::	Sm   (l+1,m+1,n+1)
	double precision,	intent(in)	::	finew(l+1,m+1,n+1)
	double precision,	intent(in)	::	fiold(l+1,m+1,n+1)
	double precision,	intent(in)	::	Tnew(l+1,m+1,n+1)
	double precision,	intent(in)	::	Told(l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFnew(l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMFold(l+1,m+1,n+1)

	integer,		intent(out)	::	nitU,nitP
	double precision,	intent(out)	::	resU,resP

!	Internal Variables
!	-----------------------------------------------------------------------------+
	integer 				::	i,j,k,alloc_stat
	double precision, 	allocatable	::	fi(:,:,:)
	double precision, 	allocatable	::	T(:,:,:)
	double precision, 	allocatable	::	SpMF(:,:,:)
	double precision, 	allocatable	::	pot(:,:,:)

! +======================================+
! | CALCULATIONS			 |
! +======================================+

! ************************************************************************
! ALLOCATE ALL ARRAYS
! ************************************************************************

	allocate(fi  (1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(T   (1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(SpMF(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(pot (1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

! ************************************************************************
! ************************************************************************

	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
	do i=1,l+1
		do j=1,m+1
			do k=1,n+1
				fi(i,j,k)=0.5d0*(fiold(i,j,k)+finew(i,j,k))
				SpMF(i,j,k)=0.5d0*(SpMFold(i,j,k)+SpMFnew(i,j,k))
				T(i,j,k)=0.5d0*(Told(i,j,k)+Tnew(i,j,k))
			enddo
		enddo
	enddo
	!$OMP END PARALLEL DO

	call Potential(l,m,n,dx,dy,dz,epn,fi,pot)

	call Momentum (l,m,n,dx,dy,dz,dt,Re,We,Fr,Bod,rGd,rGv,rGb,rOd,rOv,rOb,u2,v2,w2,u3,v3,w3,preU,preV,preW,fi,SpMF,T,pot,num_threads,nitU,resU)

	call Divergence (l,m,n,dx,dy,dz,dt,Re,rGd,rOd,u2,v2,w2,u3,v3,w3,Sm,SpMF,fi,p,num_threads,nitP,resP)

! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************
 
	deallocate(fi,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(T,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(SpMF,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(pot,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

! ************************************************************************
! ************************************************************************

return
end subroutine NS_solver

! #########################################################################
! #########################################################################

! Previously called "Pot_fine2coarse"
subroutine Potential(l,m,n,dx,dy,dz,epn,fi,pot)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz,epn

	double precision,	intent(in)	::	fi (l+1,m+1,n+1)
	double precision,	intent(out)	::	pot(l+1,m+1,n+1)


!	Internal Variables
!	-----------------------------------------------------------------------------+
	integer 				::	i,j,k
	double precision 			::	a1,a2

! +======================================+
! | CALCULATIONS			 |
! +======================================+

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,a1,a2)
do i=2,l
	do j=2,m
		do k=2,n
			a1 = (1.d0-3.d0*fi(i,j,k)+2.d0*fi(i,j,k)*fi(i,j,k))*fi(i,j,k)/2.d0
			a2 = (fi(i+1,j  ,k  )-2.d0*fi(i,j,k)+fi(i-1,j  ,k  ))/dx/dx &
			    +(fi(i  ,j+1,k  )-2.d0*fi(i,j,k)+fi(i  ,j-1,k  ))/dy/dy &
			    +(fi(i  ,j  ,k+1)-2.d0*fi(i,j,k)+fi(i  ,j  ,k-1))/dz/dz
			pot(i,j,k)= a1/epn - epn*a2
		enddo
	enddo
enddo
!$OMP END PARALLEL DO


!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(j,k)
do j=1,m+1
	do k=1,n+1
		pot(1  ,j,k)=pot(2,j,k) 
		pot(l+1,j,k)=pot(l,j,k)
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,k)
do i=1,l+1
	do k=1,n+1
		pot(i,1  ,k)=pot(i,2,k) 
		pot(i,m+1,k)=pot(i,m,k) 
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j)
do i=1,l+1
	do j=1,m+1
		pot(i,j,1  )=pot(i,j,2) 
		pot(i,j,n+1)=pot(i,j,n)
	enddo
enddo
!$OMP END PARALLEL DO

return
end subroutine Potential

! #########################################################################
! #########################################################################
   

subroutine Momentum(l,m,n,dx,dy,dz,dt,Re,We,Fr,Bod,rGd,rGv,rGb,rOd,rOv,rOb,u2,v2,w2,u3,v3,w3,preU,preV,preW,fi_mid,SpMF_m,T_m,pot,num_threads,iteration,maxRes)
 
! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n,num_threads

	double precision,	intent(in)	::	dx,dy,dz,dt
	double precision,	intent(in)	::	Re,We,Fr,Bod
	double precision,	intent(in)	::	rGd,rGv,rGb
	double precision,	intent(in)	::	rOd,rOv,rOb
	double precision,	intent(in)	::	u2(l  ,m-1,n-1)
	double precision,	intent(inout)	::	u3(l  ,m-1,n-1)
	double precision,	intent(in)	::	v2(l-1,m  ,n-1)
	double precision,	intent(inout)	::	v3(l-1,m  ,n-1)
	double precision,	intent(in)	::	w2(l-1,m-1,n  )
	double precision,	intent(inout)	::	w3(l-1,m-1,n  )

	double precision,	intent(inout)	::	preU(l  ,m-1,n-1)
	double precision,	intent(inout)	::	preV(l-1,m  ,n-1)
	double precision,	intent(inout)	::	preW(l-1,m-1,n  )

	double precision,	intent(in)	::	fi_mid(l+1,m+1,n+1) 
	double precision,	intent(in)	::	SpMF_m(l+1,m+1,n+1) 
	double precision,	intent(in)	::	T_m   (l+1,m+1,n+1) 
	double precision,	intent(in)	::	pot   (l+1,m+1,n+1) 

	integer,		intent(out)	::	iteration
	double precision,	intent(out)	::	maxRes

!	Internal variables
!	-----------------------------------------------------------------------------+
	integer					::	i,j,k

	double precision, allocatable		::	rhsU(:,:,:)
	double precision, allocatable		::	rhsV(:,:,:)
	double precision, allocatable		::	rhsW(:,:,:)

	integer					::	im1,jm1
	double precision			::	Xconv,Yconv,Zconv,Xdiff,Ydiff,Zdiff
	double precision			::	ConvNow,Diffusion,tempu,tempv,tempw
 	double precision			::	temp,ub,vb,residual,diagonal
 	double precision			::	c1,c2,c3,c4
	double precision			::	crou,cmiu,cpot,cBeta,cTemp
	double precision			::	dmdx,dmdy,dmdz
 	double precision			::	dcdx,dcdy,dcdz
 	double precision			::	dudx,dudy,dudz
 	double precision			::	dvdx,dvdy,dvdz
 	double precision			::	dwdx,dwdy,dwdz
 	double precision			::	dTdx,dTdy,dTdz
	double precision			::	mu,rho,beta,delta

	double precision, 	parameter 	:: 	etiny = 1.d-8
	double precision, 	parameter 	:: 	relax = 1.d0
	double precision, 	parameter 	:: 	tol = 5.d-9
	integer,	 	parameter 	:: 	maxit=10000

	integer					::	tid,OMP_GET_THREAD_NUM,kk,alloc_stat
	double precision			::	res_vec(0:num_threads-1)
	double precision			::	xmaxRes,ymaxRes

	
! +======================================+
! | CALCULATIONS			 |
! +======================================+

! ************************************************************************
! ALLOCATE ALL ARRAYS
! ************************************************************************

	allocate(rhsU(1:(l  ),1:(m-1),1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(rhsV(1:(l-1),1:(m  ),1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(rhsW(1:(l-1),1:(m-1),1:(n  )),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

! ************************************************************************
! ************************************************************************
 
!!--------------------------     X direction  ---------------------------------------

! Bounds of do loop change depending on whether the boundaries are periodic to non-periodic
! Currently: i bounds for non-periodicity in x

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,im1,tempv,tempw,temp,dcdx,dcdy,dcdz,dTdx,dTdy,dTdz,delta,dmdx,dmdy,dmdz,dvdx,dwdx,Xconv,Yconv,Zconv,ub,vb,Xdiff,Ydiff,Zdiff,c1,c2,c3,c4,crou,cpot,cmiu,Diffusion,ConvNow)
do i=2, l-1 !Currently: i bounds for non-periodicity in x
	do j=1, m-1
		do k=1, n-1
			!Uncomments lines below for periodic boundaries in x.
			!This also requieres changes in the do loop bounds.
			!if(i.eq.1)then
			!  	im1=l-1
			!else
				im1=i-1
			!endif

			tempv=(v2(im1,j,k)+v2(i,j,k)+v2(im1,j+1,k  )+v2(i,j+1,k  ))/4.d0
			tempw=(w2(im1,j,k)+w2(i,j,k)+w2(im1,j  ,k+1)+w2(i,j  ,k+1))/4.d0

			temp =0.5d0*(u2(i+1,j,k)-u2(im1,j,k))/dx
			Xconv=u2(i,j,k)*temp
			Xdiff=(u2(i+1,j,k)-2.d0*u2(i,j,k)+u2(im1,j,k))/dx/dx

			dcdx= (fi_mid(i+1,j+1,k+1)-fi_mid(i,j+1,k+1))/dx

			c1 = mu(rGv,rOv,fi_mid(i+1,j+1,k+1),SpMF_m(i+1,j+1,k+1))
			c2 = mu(rGv,rOv,fi_mid(i  ,j+1,k+1),SpMF_m(i  ,j+1,k+1))
			dmdx = (c1-c2)/dx
			
			! dudx=temp           
			dmdx=2.d0*dmdx*temp

			if(j==1)then
				ub=u2(i,j+1,k)
				vb=u2(i,j  ,k)
			elseif(j==m-1)then
				ub=u2(i,j  ,k)
				vb=u2(i,j-1,k)
			else
				ub=u2(i,j+1,k)
				vb=u2(i,j-1,k)
			endif

			temp=0.5d0*(ub-vb)/dy
			Yconv=tempv*temp
			Ydiff=(ub-2.d0*u2(i,j,k)+vb)/dy/dy

			c2 = (fi_mid(i+1,j+2,k+1)+fi_mid(i,j+2,k+1)+fi_mid(i+1,j+1,k+1)+fi_mid(i,j+1,k+1))/4.d0
			c1 = (fi_mid(i+1,j  ,k+1)+fi_mid(i,j  ,k+1)+fi_mid(i+1,j+1,k+1)+fi_mid(i,j+1,k+1))/4.d0
			dcdy= (c2-c1)/dy

			dvdx = (v2(i,j,k)+v2(i,j+1,k)-v2(im1,j,k)-v2(im1,j+1,k))/2.d0/dx

			c1 = mu(rGv,rOv,fi_mid(i+1,j  ,k+1),SpMF_m(i+1,j  ,k+1))
			c2 = mu(rGv,rOv,fi_mid(i  ,j  ,k+1),SpMF_m(i  ,j  ,k+1))
			c3 = mu(rGv,rOv,fi_mid(i+1,j+2,k+1),SpMF_m(i+1,j+2,k+1))
			c4 = mu(rGv,rOv,fi_mid(i  ,j+2,k+1),SpMF_m(i  ,j+2,k+1))
			dmdy = (c4+c3-c2-c1)/4.d0/dy

			! dudy=temp           
			dmdy=dmdy*(temp+dvdx)

			if(k==1)then
				temp=(u2(i,j,k)+u2(i,j,k+1))/(2.d0*dz)
			elseif(k==n-1)then
				temp=(u2(i,j,k)-u2(i,j,k-1))/(2.d0*dz)
			else
				ub=u2(i,j,k+1)
				vb=u2(i,j,k-1)
				temp=0.5d0*(ub-vb)/dz
			endif

			! dudz=temp           
			Zconv=tempw*temp

			c2 = (fi_mid(i+1,j+1,k+2)+ fi_mid(i,j+1,k+2)+ fi_mid(i+1,j+1,k+1)+ fi_mid(i,j+1,k+1))/4.d0
			c1 = (fi_mid(i+1,j+1,k)  + fi_mid(i,j+1,k)  + fi_mid(i+1,j+1,k+1)+ fi_mid(i,j+1,k+1))/4.d0

			dcdz= (c2-c1)/dz

			dwdx= (w2(i,j,k)+w2(i,j,k+1)-w2(im1,j,k)-w2(im1,j,k+1))/2.d0/dx

			c1 = mu(rGv,rOv,fi_mid(i+1,j+1,k  ),SpMF_m(i+1,j+1,k  ))
			c2 = mu(rGv,rOv,fi_mid(i  ,j+1,k  ),SpMF_m(i  ,j+1,k  ))
			c3 = mu(rGv,rOv,fi_mid(i+1,j+1,k+2),SpMF_m(i+1,j+1,k+2))
			c4 = mu(rGv,rOv,fi_mid(i  ,j+1,k+2),SpMF_m(i  ,j+1,k+2))
			dmdz = (c4+c3-c2-c1)/4.d0/dz

			! dudz=temp
			dmdz=dmdz*(temp+dwdx)

			if(k==1)then
				ub = 0.d0
  				Zdiff=(u2(i,j,k+1)-3.d0*u2(i,j,k)+2.d0*ub)/dz/dz
			elseif(k==n-1)then
  				Zdiff=(-u2(i,j,k)+u2(i,j,k-1))/dz/dz
			else
  				Zdiff=(u2(i,j,k+1)-2.d0*u2(i,j,k)+u2(i,j,k-1))/dz/dz	
			endif

			c1 = (fi_mid(i,j+1,k+1)+fi_mid(i+1,j+1,k+1))/2.d0
			c2 = (SpMF_m(i,j+1,k+1)+SpMF_m(i+1,j+1,k+1))/2.d0
	
! 			if(c1.lt.0.d0)then
!   				c1 = 0.d0
! 			elseif(c1.gt.1.d0)then
!   				c1 = 1.d0
! 			endif
! 
! 			if(c2.lt.0.d0)then
!   				c2 = 0.d0
! 			elseif(c2.gt.1.d0)then
!   				c2 = 1.d0
! 			endif

			crou = rho(rGd,rOd,c1,c2)
			cmiu = mu (rGv,rOv,c1,c2)
			
			Diffusion=cmiu*(Xdiff+Ydiff+Zdiff)/Re/crou

			ConvNow=Xconv+Yconv+Zconv-(dmdx+dmdy+dmdz)/Re/crou

			cpot = (pot(i,j+1,k+1)+pot(i+1,j+1,k+1))/2.d0
		
			dTdx = ( T_m(i+1,j+1,k+1)-T_m(i,j+1,k+1))/dx
		
			dTdy = ( T_m(i+1,j+2,k+1) &
				+T_m(i  ,j+2,k+1) &
				-T_m(i+1,j  ,k+1) &
				-T_m(i  ,j  ,k+1) )/4.d0/dy
		
			dTdz = ( T_m(i  ,j+1,k+2) &
				+T_m(i+1,j+1,k+2) &
				-T_m(i  ,j+1,k  ) &
				-T_m(i+1,j+1,k  ) )/4.d0/dz

			delta = dsqrt(dcdx*dcdx + dcdy*dcdy + dcdz*dcdz)

! 			if (delta.gt.0.d0)then
! 				etiny = 0.d0
! 			else
! 				etiny = 1.d0
! 			endif

			!temp = (cpot*dcdx/We - (delta*dTdx-(1.d0/(delta+etiny))*(dcdx*dcdx*dTdx+dcdx*dcdy*dTdy+dcdx*dcdz*dTdz))/Re)/crou
			temp = (cpot*dcdx/We - (delta*dTdx-(1.d0/(delta+etiny))*(dcdx*dcdx*dTdx+dcdx*dcdy*dTdy+dcdx*dcdz*dTdz))/Re)/crou
			
			rhsu(i,j,k)=-(1.5d0*ConvNow-0.5d0*preU(i,j,k))+ 0.5d0*Diffusion+u2(i,j,k)/dt+temp

			preU(i,j,k)=ConvNow
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!!--------------------------     Y direction  ---------------------------------------

! Bounds of do loop change depending on whether the boundaries are periodic to non-periodic
! Currently: j bounds for non-periodicity in y

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,jm1,tempu,tempw,temp,Xconv,Yconv,Zconv,Xdiff,Ydiff,Zdiff,dcdx,dcdy,dcdz,dmdx,dmdy,dmdz,ub,vb,c1,c2,c3,c4,dudy,dwdy,dTdx,dTdy,dTdz,delta,cpot,crou,cmiu,ConvNow,Diffusion)
do i=1, l-1
	do j=2, m-1! Currently: j bounds for non-periodicity in y
		do k=1, n-1

			!Uncomments lines below for periodic boundaries in x.
			!This also requieres changes in the do loop bounds.
			!if(j.eq.1)then
			!	jm1=m-1
			!else
				jm1=j-1
			!endif

			tempu = (u2(i,jm1,k)+u2(i,j,k)+u2(i+1,jm1,k  )+u2(i+1,j,k  ))/4.d0
			tempw = (w2(i,jm1,k)+w2(i,j,k)+w2(i  ,jm1,k+1)+w2(i  ,j,k+1))/4.d0

			temp = 0.5d0*(v2(i,j+1,k)-v2(i,jm1,k))/dy

			Yconv = v2(i,j,k)*temp
			Ydiff =(v2(i,j+1,k)-2.d0*v2(i,j,k)+v2(i,jm1,k))/dy/dy

			dcdy = (fi_mid(i+1,j+1,k+1)-fi_mid(i+1,j,k+1))/dy
			
			c1 = mu(rGv,rOv,fi_mid(i+1,j+1,k+1),SpMF_m(i+1,j+1,k+1))
			c2 = mu(rGv,rOv,fi_mid(i+1,j  ,k+1),SpMF_m(i+1,j  ,k+1))
			dmdy = (c1-c2)/dy
			
			!temp=dvdy
			dmdy = 2.d0*dmdy*temp

			if(i==1)then
				ub = v2(i+1,j,k)
				vb = v2(i,j,k)
			elseif(i==l-1)then
				ub = v2(i,j,k)
				vb = v2(i-1,j,k)
			else
				ub = v2(i+1,j,k)
				vb = v2(i-1,j,k)
			endif

			temp=0.5d0*(ub-vb)/dx
			
			Xconv=tempu*temp
			Xdiff=(ub-2.d0*v2(i,j,k)+vb)/dx/dx

			c2 = (fi_mid(i+2,j+1,k+1)+fi_mid(i+2,j,k+1)+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j,k+1))/4.d0
			c1 = (fi_mid(i  ,j+1,k+1)+fi_mid(i  ,j,k+1)+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j,k+1))/4.d0
			dcdx = (c2-c1)/dx

			dudy = (u2(i,j,k)+u2(i+1,j,k)-u2(i,jm1,k)-u2(i+1,jm1,k))/2.d0/dy

			c1 = mu(rGv,rOv,fi_mid(i  ,j  ,k+1),SpMF_m(i  ,j  ,k+1))
			c2 = mu(rGv,rOv,fi_mid(i  ,j+1,k+1),SpMF_m(i  ,j+1,k+1))
			c3 = mu(rGv,rOv,fi_mid(i+2,j  ,k+1),SpMF_m(i+2,j  ,k+1))
			c4 = mu(rGv,rOv,fi_mid(i+2,j+1,k+1),SpMF_m(i+2,j+1,k+1))
			dmdx = (c4+c3-c2-c1)/4.d0/dx

			! temp = dvdx
			dmdx = dmdx*(temp+dudy)


			if(k==1)then
				temp=(v2(i,j,k+1)+v2(i,j,k))/(2.d0*dz)
			elseif(k==n-1)then
				temp=(v2(i,j,k)-v2(i,j,k-1))/(2.d0*dz)
			else
				temp=0.5d0*(v2(i,j,k+1)-v2(i,j,k-1))/dz
			endif

			Zconv=tempw*temp

			dwdy= (w2(i,j,k)+w2(i,j,k+1) -w2(i,jm1,k)-w2(i,jm1,k+1))/2.d0/dy

			c2  = (fi_mid(i+1,j+1,k+2) +fi_mid(i+1,j,k+2)+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j,k+1))/4.d0
			c1  = (fi_mid(i+1,j+1,k  ) +fi_mid(i+1,j,k  )+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j,k+1))/4.d0
			dcdz= (c2-c1)/dz

			c1 = mu(rGv,rOv,fi_mid(i+1,j+1,k  ),SpMF_m(i+1,j+1,k  ))
			c2 = mu(rGv,rOv,fi_mid(i+1,j  ,k  ),SpMF_m(i+1,j  ,k  ))
			c3 = mu(rGv,rOv,fi_mid(i+1,j+1,k+2),SpMF_m(i+1,j+1,k+2))
			c4 = mu(rGv,rOv,fi_mid(i+1,j  ,k+2),SpMF_m(i+1,j  ,k+2))
			dmdz = (c4+c3-c2-c1)/4.d0/dz

			! temp=dvdz
			dmdz=dmdz*(temp+dwdy)

			if(k==1)then
				ub = 0.d0
				Zdiff=(v2(i,j,k+1)-3.d0*v2(i,j,k)+2.d0*ub)/dz/dz
			elseif(k==n-1)then
				Zdiff=(-v2(i,j,k)+v2(i,j,k-1))/dz/dz
			else
				Zdiff=(v2(i,j,k+1)-2.d0*v2(i,j,k)+v2(i,j,k-1))/dz/dz
			endif

			c1 = (fi_mid(i+1,j  ,k+1)+fi_mid(i+1,j+1,k+1))/2.d0
			c2 = (SpMF_m(i+1,j  ,k+1)+SpMF_m(i+1,j+1,k+1))/2.d0
	
! 			if(c1.lt.0.d0)then
!   				c1 = 0.d0
! 			elseif(c1.gt.1.d0)then
!   				c1 = 1.d0
! 			endif
! 
! 			if(c2.lt.0.d0)then
!   				c2 = 0.d0
! 			elseif(c2.gt.1.d0)then
!   				c2 = 1.d0
! 			endif

			crou = rho(rGd,rOd,c1,c2)
			cmiu = mu (rGv,rOv,c1,c2)
			
			ConvNow=Xconv+Yconv+Zconv-(dmdx+dmdy+dmdz)/Re/crou
			Diffusion=cmiu*(Xdiff+Ydiff+Zdiff)/crou/Re

			cpot = (pot(i+1,j,k+1)+pot(i+1,j+1,k+1))/2.d0
		
			dTdx = ( T_m(i+2,j+1,k+1) &
				+T_m(i+2,j  ,k+1) &
				-T_m(i  ,j+1,k+1) &
				-T_m(i  ,j  ,k+1) )/4.d0/dx

			dTdy = ( T_m(i+1,j+1,k+1)-T_m(i+1,j  ,k+1))/dy
		
			dTdz = ( T_m(i+1,j  ,k+2) &
				+T_m(i+1,j+1,k+2) &
				-T_m(i+1,j  ,k  ) &
				-T_m(i+1,j+1,k  ) )/4.d0/dz

			delta = dsqrt(dcdx*dcdx + dcdy*dcdy + dcdz*dcdz)

! 			if (delta.gt.0.d0)then
! 				etiny = 0.d0
! 			else
! 				etiny = 1.d0
! 			endif
		
			!temp = (cpot*dcdy/We - (delta*dTdy-(1.d0/(delta+etiny))*(dcdy*dcdx*dTdx+dcdy*dcdy*dTdy+dcdy*dcdz*dTdz))/Re)/crou
			temp = (cpot*dcdy/We - (delta*dTdy-(1.d0/(delta+etiny))*(dcdy*dcdx*dTdx+dcdy*dcdy*dTdy+dcdy*dcdz*dTdz))/Re)/crou
				
			rhsV(i,j,k)=-(1.5d0*ConvNow-0.5d0*preV(i,j,k))+0.5*Diffusion+v2(i,j,k)/dt+temp

			preV(i,j,k)=ConvNow
		enddo
	enddo
enddo
!$OMP END PARALLEL DO
!           write(*,*)'Y direction momentum done'

!!--------------------------     Z direction  ---------------------------------------

! Bounds of do loop change depending on whether the boundaries are periodic to non-periodic
! Currently: k bounds for non-periodicity in k

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,tempu,tempv,ub,vb,temp,Zconv,Zdiff,dcdz,dmdz,Xconv,Xdiff,c4,c3,c2,c1,dudz,dcdx,dmdx,Yconv,Ydiff,dvdz,dcdy,dmdy,dTdx,dTdy,dTdz,cpot,crou,cmiu,cTemp,cBeta,delta,ConvNow,Diffusion)
do i=1, l-1
	do j=1, m-1
		do k=2, n-1 ! Currently: k bounds for non-periodicity in k

			tempu=(u2(i,j,k-1)+u2(i,j,k)+u2(i+1,j  ,k-1)+u2(i+1,j  ,k))/4.d0
			tempv=(v2(i,j,k-1)+v2(i,j,k)+v2(i  ,j+1,k-1)+v2(i  ,j+1,k))/4.d0

			ub = w2(i,j,k+1)
			vb = w2(i,j,k-1)
			temp = 0.5d0*(ub-vb)/dz

			Zconv = w2(i,j,k)*temp
			Zdiff = (ub-2.d0*w2(i,j,k)+vb)/dz/dz
 
			dcdz= (fi_mid(i+1,j+1,k+1)-fi_mid(i+1,j+1,k))/dz

			c1 = mu(rGv,rOv,fi_mid(i+1,j+1,k+1),SpMF_m(i+1,j+1,k+1))
			c2 = mu(rGv,rOv,fi_mid(i+1,j+1,k  ),SpMF_m(i+1,j+1,k  ))
			dmdz = (c1-c2)/dz
			
			! temp = dwdz
			dmdz = 2.d0*dmdz*temp

			if(i==1)then
				ub = w2(i+1,j,k)
				vb = w2(i  ,j,k)
			elseif(i==l-1)then
				ub = w2(i  ,j,k)
				vb = w2(i-1,j,k)
			else
				ub=w2(i+1,j,k)
				vb=w2(i-1,j,k)
			endif

			temp = 0.5d0*(ub-vb)/dx

			Xconv = tempu*temp
			Xdiff = (ub-2.d0*w2(i,j,k)+vb)/dx/dx

			c2 = (fi_mid(i+2,j+1,k+1)+fi_mid(i+2,j+1,k)+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j+1,k))/4.d0
			c1 = (fi_mid(i  ,j+1,k+1)+fi_mid(i  ,j+1,k)+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j+1,k))/4.d0
			dcdx= (c2-c1)/dx

			dudz= (u2(i,j,k)+u2(i+1,j,k)-u2(i,j,k-1)-u2(i+1,j,k-1))/2.d0/dz

			c4 = mu(rGv,rOv,fi_mid(i+2,j+1,k+1),SpMF_m(i+2,j+1,k+1))
			c3 = mu(rGv,rOv,fi_mid(i+2,j+1,k  ),SpMF_m(i+2,j+1,k  ))
			c2 = mu(rGv,rOv,fi_mid(i  ,j+1,k+1),SpMF_m(i  ,j+1,k+1))
			c1 = mu(rGv,rOv,fi_mid(i  ,j+1,k  ),SpMF_m(i  ,j+1,k  ))
			dmdx = (c4+c3-c2-c1)/4.d0/dx

			! temp = dwdx
			dmdx=dmdx*(temp+dudz)

			if(j==1)then
				ub = w2(i,j+1,k)
				vb = w2(i,j  ,k)
			elseif(j==m-1)then
				ub = w2(i,j  ,k)
				vb = w2(i,j-1,k)
			else
				ub = w2(i,j+1,k)
				vb = w2(i,j-1,k)
			endif
			temp = 0.5d0*(ub-vb)/dy

			Yconv= temp*tempv
			Ydiff= (ub-2.d0*w2(i,j,k)+vb)/dy/dy


			c2  = (fi_mid(i+1,j+2,k+1)+fi_mid(i+1,j+2,k)+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j+1,k))/4.d0
			c1  = (fi_mid(i+1,j  ,k+1)+fi_mid(i+1,j  ,k)+fi_mid(i+1,j+1,k+1)+fi_mid(i+1,j+1,k))/4.d0
			dcdy= (c2-c1)/dy

			dvdz= (v2(i,j,k)+v2(i,j+1,k)-v2(i,j,k-1)-v2(i,j+1,k-1))/2.d0/dz

			c4 = mu(rGv,rOv,fi_mid(i+1,j+2,k+1),SpMF_m(i+1,j+2,k+1))
			c3 = mu(rGv,rOv,fi_mid(i+1,j+2,k  ),SpMF_m(i+1,j+2,k  ))
			c2 = mu(rGv,rOv,fi_mid(i+1,j  ,k+1),SpMF_m(i+1,j  ,k+1))
			c1 = mu(rGv,rOv,fi_mid(i+1,j  ,k  ),SpMF_m(i+1,j  ,k  ))
			dmdy = (c4+c3-c2-c1)/4.d0/dy
			
			! temp=dwdy
			dmdy = dmdy*(temp+dvdz)

			c1 = (fi_mid(i+1,j+1,k  )+fi_mid(i+1,j+1,k+1))/2.d0
			c2 = (SpMF_m(i+1,j+1,k  )+SpMF_m(i+1,j+1,k+1))/2.d0
	
! 			if(c1.lt.0.d0)then
!   				c1 = 0.d0
! 			elseif(c1.gt.1.d0)then
!   				c1 = 1.d0
! 			endif
! 
! 			if(c2.lt.0.d0)then
!   				c2 = 0.d0
! 			elseif(c2.gt.1.d0)then
!   				c2 = 1.d0
! 			endif

			crou  = rho (rGd,rOd,c1,c2)
			cmiu  = mu  (rGv,rOv,c1,c2)
			cBeta = beta(rGb,rOb,c1,c2)
			cTemp = (T_m(i+1,j+1,k)+T_m(i+1,j+1,k+1))/2.d0

			ConvNow=Xconv+Yconv+Zconv-(dmdx+dmdy+dmdz)/Re/crou
			Diffusion=cmiu*(Xdiff+Ydiff+Zdiff)/crou/Re

			cpot  = (pot(i+1,j+1,k)+pot(i+1,j+1,k+1))/2.d0

			dTdx = ( T_m(i+2,j+1,k+1) &
				+T_m(i+2,j+1,k  ) &
				-T_m(i  ,j+1,k+1) &
				-T_m(i  ,j+1,k  ) )/4.d0/dx

			dTdy = ( T_m(i+1,j+2,k  ) &
				+T_m(i+1,j+2,k+1) &
				-T_m(i+1,j  ,k+1) &
				-T_m(i+1,j  ,k  ) )/4.d0/dy

			dTdz = ( T_m(i+1,j+1,k+1)-T_m(i+1,j+1,k  ))/dz
		
			delta = dsqrt(dcdx*dcdx + dcdy*dcdy + dcdz*dcdz)

! 			if (delta.gt.0.d0)then
! 				etiny = 0.d0
! 			else
! 				etiny = 1.d0
! 			endif

! 			temp = (cpot*dcdz/We - (delta*dTdz-(1.d0/(delta+etiny))*(dcdz*dcdx*dTdx+dcdz*dcdy*dTdy+dcdz*dcdz*dTdz))/Re &
! 			- crou*(1.d0/Fr - Bod*cBeta*cTemp/Re))/crou
			temp = (cpot*dcdz/We - (delta*dTdz-(1.d0/(delta+etiny))*(dcdz*dcdx*dTdx+dcdz*dcdy*dTdy+dcdz*dcdz*dTdz))/Re &
			- crou*(1.d0/Fr - Bod*cBeta*cTemp/Re))/crou
				
			rhsW(i,j,k)=-(1.5d0*ConvNow-0.5d0*preW(i,j,k))+0.5d0*Diffusion+w2(i,j,k)/dt+temp

			preW(i,j,k)=ConvNow          
		enddo
	enddo
enddo
!$OMP END PARALLEL DO


!+++++++++++++++  End of the source term computation for the momentum equation  +++++++++     
  
!+++++++++++++++  Start iteration to obtain converged intermediate velocity   ++++++++++
  
diagonal =  (1.d0/dx/dx+1.d0/dy/dy+1.d0/dz/dz)/Re
  
do iteration = 1, maxit

	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,kk,im1,ub,vb,Xdiff,Ydiff,Zdiff,c1,c2,crou,cmiu,Diffusion,residual,maxRes,tid)

	tid=OMP_GET_THREAD_NUM()
	maxRes=0.d0

	!$OMP DO
	do i=2, l-1 !Currently: i bounds for non-periodicity in x
		do j=1, m-1
			do kk=1, n-1

				k = kk
				if(mod(iteration,2).eq.0) k=n-k
		
				!Uncomments lines below for periodic boundaries in x.
				!This also requieres changes in the do loop bounds.
				!if(i.eq.1)then
				!   	im1=l-1
				!else
					im1=i-1
				!endif

				Xdiff=(u3(i+1,j,k)-2.d0*u3(i,j,k)+u3(im1,j,k))/dx/dx
				!write(*,*) 'iter=',iteration,'xyz',i,j,k,'Xdiff done'

				if(j==1)then
					ub = u3(i,j+1,k)
					vb = u3(i,j  ,k)
				elseif(j==m-1)then
					ub = u3(i,j  ,k)
					vb = u3(i,j-1,k)
				else
					ub = u3(i,j+1,k)
					vb = u3(i,j-1,k)
				endif

				Ydiff=(ub-2.d0*u3(i,j,k)+vb)/dy/dy
				!write(*,*) 'iter=',iteration,'xyz',i,j,k,'Ydiff done'

				if(k==1)then
					ub = 0.d0
					Zdiff=(u3(i,j,k+1)-3.d0*u3(i,j,k)+2.d0*ub)/dz/dz
				elseif(k==n-1)then
					Zdiff=(-u3(i,j,k)+u3(i,j,k-1))/dz/dz
				else
					Zdiff=(u3(i,j,k+1)-2.d0*u3(i,j,k)+u3(i,j,k-1))/dz/dz
				endif
				!write(*,*) 'iter=',iteration,'xyz',i,j,k,'Zdiff done'

				c1 = (fi_mid(i,j+1,k+1)+fi_mid(i+1,j+1,k+1))/2.d0
				c2 = (SpMF_m(i,j+1,k+1)+SpMF_m(i+1,j+1,k+1))/2.d0
	
! 				if(c1.lt.0.d0)then
! 					c1 = 0.d0
! 				elseif(c1.gt.1.d0)then
! 					c1 = 1.d0
! 				endif
! 	
! 				if(c2.lt.0.d0)then
! 					c2 = 0.d0
! 				elseif(c2.gt.1.d0)then
! 					c2 = 1.d0
! 				endif

				crou = rho(rGd,rOd,c1,c2)
				cmiu = mu (rGv,rOv,c1,c2)

				Diffusion=cmiu*(Xdiff+Ydiff+Zdiff)/Re/crou

				residual = rhsu(i,j,k)+Diffusion*0.5d0-u3(i,j,k)/dt
				maxRes = dmax1(maxRes,dabs(residual))
				u3(i,j,k)=u3(i,j,k)+relax*residual/(cmiu*diagonal/crou+1.d0/dt)

			enddo
		enddo
	enddo
	!$OMP END DO

	res_vec(tid)=maxRes
	!write(*,*) 'tid',tid,' maxRes',maxRes

	!$OMP END PARALLEL

	maxRes=0.d0

	do tid=0,num_threads-1
		!write(*,*) 'num_threads',num_threads,'tid',tid,' res_vec',res_vec(tid)
		if(res_vec(tid).gt.maxRes)then
			maxRes=res_vec(tid)
		endif
	end do

	xmaxRes=maxRes
	!write(*,*)'xconv maxRes here ',maxRes,' iter ',iteration



	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,kk,jm1,Xdiff,Ydiff,Zdiff,ub,vb,c1,c2,crou,cmiu,Diffusion,residual,maxRes,tid)

	tid=OMP_GET_THREAD_NUM()
	maxRes=xmaxRes

	!$OMP DO
	do i=1, l-1
		do j=2, m-1 !Currently: j bounds for non-periodicity in y
			do kk=1, n-1
				k = kk
				if(mod(iteration,2).eq.0) k=n-k

				!Uncomments lines below for periodic boundaries in Y.
				!This also requieres changes in the do loop bounds.
				!if(j.eq.1)then
				!	jm1=m-1
				!else
					jm1=j-1
				!endif

				Ydiff=(v3(i,j+1,k)-2.d0*v3(i,j,k)+v3(i,jm1,k))/dy/dy

				if(i==1)then
					ub = v3(i+1,j,k)
					vb = v3(i  ,j,k)
				elseif(i==l-1)then
					ub = v3(i  ,j,k)
					vb = v3(i-1,j,k)
				else
					ub = v3(i+1,j,k)
					vb = v3(i-1,j,k)
				endif
				Xdiff=(ub-2.d0*v3(i,j,k)+vb)/dx/dx

				if(k==1)then
					ub = 0.d0
					Zdiff=(v3(i,j,k+1)-3.d0*v3(i,j,k)+2.d0*ub)/dz/dz
				elseif(k==n-1)then
					Zdiff=(-v3(i,j,k)+v3(i,j,k-1))/dz/dz
				else
					Zdiff=(v3(i,j,k+1)-2.d0*v3(i,j,k)+v3(i,j,k-1))/dz/dz
				endif

				c1 = (fi_mid(i+1,j  ,k+1)+fi_mid(i+1,j+1,k+1))/2.d0
				c2 = (SpMF_m(i+1,j  ,k+1)+SpMF_m(i+1,j+1,k+1))/2.d0
	
! 				if(c1.lt.0.d0)then
!   					c1 = 0.d0
! 				elseif(c1.gt.1.d0)then
!   					c1 = 1.d0
! 				endif
! 
! 				if(c2.lt.0.d0)then
!   					c2 = 0.d0
! 				elseif(c2.gt.1.d0)then
!   					c2 = 1.d0
! 				endif

				crou  = rho (rGd,rOd,c1,c2)
				cmiu  = mu  (rGv,rOv,c1,c2)


				Diffusion=cmiu*(Xdiff+Ydiff+Zdiff)/Re/crou

				residual = rhsv(i,j,k)+Diffusion*0.5d0-v3(i,j,k)/dt
				maxRes = dmax1(maxRes,dabs(residual))

				v3(i,j,k)=v3(i,j,k)+relax*residual/(cmiu*diagonal/crou+1.d0/dt)
			enddo
		enddo
	enddo
	!$OMP END DO

	res_vec(tid)=maxRes

	!$OMP END PARALLEL

	maxRes=0.d0

	do tid=0,num_threads-1
		if(res_vec(tid).gt.maxRes)then
			maxRes=res_vec(tid)
		endif
	enddo
	ymaxRes=maxRes

	!write(*,*)'yconv maxRes here ',maxRes,' iter ',iteration

	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,kk,ub,vb,Xdiff,Ydiff,Zdiff,c1,c2,crou,cmiu,Diffusion,residual,maxRes,tid)

	tid=OMP_GET_THREAD_NUM()
	maxRes=ymaxRes

	!$OMP DO 
	do i=1, l-1
		do j=1, m-1
			do kk=2, n-1 !Currently: kk bounds for non-periodicity in z
				k = kk
				if(mod(iteration,2).eq.0) k=n+1-k
				
				ub = w3(i,j,k+1)
				vb = w3(i,j,k-1) 
				Zdiff=(ub-2.d0*w3(i,j,k)+vb)/dz/dz

				if(i==1)then
					ub = w3(i+1,j,k)
					vb = w3(i  ,j,k)
				elseif(i==l-1)then
					ub = w3(i  ,j,k)
					vb = w3(i-1,j,k)
				else
					ub = w3(i+1,j,k)
					vb = w3(i-1,j,k)
				endif
				Xdiff=(ub-2.d0*w3(i,j,k)+vb)/dx/dx

				if(j==1)then
					ub = w3(i,j+1,k)
					vb = w3(i,j  ,k)
				elseif(j==m-1)then
					ub = w3(i,j ,k)
					vb = w3(i,j-1,k)
				else
					ub = w3(i,j+1,k)
					vb = w3(i,j-1,k)
				endif
				Ydiff=(ub-2.d0*w3(i,j,k)+vb)/dy/dy

				c1 = (fi_mid(i+1,j+1,k  )+fi_mid(i+1,j+1,k+1))/2.d0
				c2 = (SpMF_m(i+1,j+1,k  )+SpMF_m(i+1,j+1,k+1))/2.d0
	
! 				if(c1.lt.0.d0)then
!   					c1 = 0.d0
! 				elseif(c1.gt.1.d0)then
!   					c1 = 1.d0
! 				endif
! 
! 				if(c2.lt.0.d0)then
!   					c2 = 0.d0
! 				elseif(c2.gt.1.d0)then
!   					c2 = 1.d0
! 				endif

				crou  = rho (rGd,rOd,c1,c2)
				cmiu  = mu  (rGv,rOv,c1,c2)


				Diffusion=cmiu*(Xdiff+Ydiff+Zdiff)/Re/crou

				residual = rhsw(i,j,k)+Diffusion*0.5d0-w3(i,j,k)/dt
				maxRes = dmax1(maxRes,dabs(residual))

				w3(i,j,k)=w3(i,j,k)+relax*residual/(cmiu*diagonal/crou+1.d0/dt)

			enddo
		enddo
	enddo
	!$OMP END DO
	res_vec(tid)=maxRes
	!$OMP END PARALLEL

	maxRes=0.d0

	do tid=0,num_threads-1
		if(res_vec(tid).gt.maxRes)then
			maxRes=res_vec(tid)
		endif
	enddo

	! write(*,*)'conv. int. vel. part 3 done'
	! write(*,*)'iteration=',iteration
	! write(*,*)'zconv maxRes here ',maxRes,' iter ',iteration


	do j=1,m-1
		do k=1,n-1
			u3(l,j,k)=u3(l-1,j,k)
			u3(1,j,k)=u3(2,j,k)
		enddo
	enddo

	do i=1,l-1
		do k=1,n-1
			v3(i,1,k)=v3(i,2,k)
			v3(i,m,k)=v3(i,m-1,k)
		enddo
	enddo

	do i=1,l-1
		do j=1,m-1
			w3(i,j,1)=0.d0
			w3(i,j,n)=v3(i,j,n-1)
		enddo
	enddo

	if(maxRes.le.tol) go to 1 

enddo

1 continue

!write(*,*)'iteration number=',iteration
!write(*,*)'Momentum maxRes=',maxRes        

! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************
 
	deallocate(rhsU,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(rhsV,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(rhsW,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

! ************************************************************************
! ************************************************************************

return
end subroutine Momentum
  
! #########################################################################
! #########################################################################


!       The following subroutine is used to confirm the divergence free of
!       the flow field
subroutine FlowFieldChecking(u,v,w,l,m,n,dx,dy,dz,maxError)
  implicit none
  integer ::l,m,n
  double precision ::dx,dy,dz,maxError
  double precision ::u(l,m-1,n-1),v(l-1,m,n-1),w(l-1,m-1,n)
  
  integer ::i,j,k
  double precision ::residual
  
  maxError=0.d0
  do i=1,l-1
     do j=1,m-1
        do k=1,n-1
           residual=(u(i+1,j,k)-u(i,j,k))/dx+(v(i,j+1,k)-v(i,j,k))/dy+(w(i,j,k+1)-w(i,j,k))/dz
           maxError=dmax1(maxError,dabs(residual))
        enddo
     enddo
  enddo
  
  return
end subroutine FlowFieldChecking

! #########################################################################
! #########################################################################
       
function mu(rGv,rOv,fi,SpMF)

!	Function arguments
!	-----------------------------------------------------------------------------+
	implicit none
	double precision		::	mu,rGv,rOv,fi,SpMF

!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision 		::	mub

	mub = (1.d0-SpMF)/rGv + SpMF/rOv

	mu  = fi + mub*(1.d0-fi)

return
end function mu

! #########################################################################
! #########################################################################
       
function rho(rGd,rOd,fi,SpMF)

!	Function arguments
!	-----------------------------------------------------------------------------+
	implicit none
	double precision		::	rho,rGd,rOd,fi,SpMF

!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision 		::	rhob

	rhob = 1.d0/((1.d0-SpMF)*rGd + SpMF*rOd)

	rho  = fi + rhob*(1.d0-fi)

return
end function rho

! #########################################################################
! #########################################################################
       
function beta(rGb,rOb,fi,SpMF)

!	Function arguments
!	-----------------------------------------------------------------------------+
	implicit none
	double precision		::	beta,rGb,rOb,fi,SpMF

!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision 		::	betab

	betab = (1.d0-SpMF)/rGb + SpMF/rOb

	beta  = fi + betab*(1.d0-fi)

return
end function beta
