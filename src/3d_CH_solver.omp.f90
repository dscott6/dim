subroutine WENOAppoxConvection(fi,u,v,w,l,m,n,dx,dy,dz,RHS)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz

	double precision,	intent(in)	::	fi(l+1, m+1, n+1)
	double precision,	intent(in)	::	u (l  , m-1, n-1)
	double precision,	intent(in)	::	v (l-1, m  , n-1)
	double precision,	intent(in)	::	w (l-1, m-1, n  )

	double precision,	intent(out)	::	RHS(l+1, m+1, n+1)

!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision, allocatable		::	dfx(:,:,:)
	double precision, allocatable		::	dfy(:,:,:)
	double precision, allocatable		::	dfz(:,:,:)

	integer					::	i,j,k

	double precision			::	v1,v2,v3,v4,v5

	double precision			::	WENO5
	
	integer					::	alloc_stat


! +======================================+
! | CALCULATIONS			 |
! +======================================+

! ************************************
! ALLOCATE ALL ARRAYS
! ************************************

	! Dimensions
	! dfx(l  , m-1, n-1)
	! dfy(l-1, m,   n-1)
	! dfz(l-1, m-1, n  )
	allocate(dfx(1:l,1:(m-1),1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(dfy(1:(l-1),1:m,1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(dfz(1:(l-1),1:(m-1),1:n),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	
	
! ************************************
! ************************************



!---------------------------- X DIRECTION---------------------------------------

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,v1,v2,v3,v4,v5)
do i=1,l
	do j=1,m-1
		do k=1,n-1

			if((i.ge.4) .and. (i.le.(l-3)))then !C I

				if(u(i,j,k).ge.0.d0)then
					! Stencil 1 (3+2)
					v1=fi(i-2,j+1,k+1)
					v2=fi(i-1,j+1,k+1)
					v3=fi(i  ,j+1,k+1)
					v4=fi(i+1,j+1,k+1)
					v5=fi(i+2,j+1,k+1)
			
				else
					! Stencil 2 (2+3)
					v1=fi(i+3,j+1,k+1)
					v2=fi(i+2,j+1,k+1)
					v3=fi(i+1,j+1,k+1)
					v4=fi(i  ,j+1,k+1)
					v5=fi(i-1,j+1,k+1)
				endif

				dfx(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			elseif((i.eq.3) .and. (u(i,j,k).lt.0.d0))then !C II

				! Stencil 2 (2+3)
				v1=fi(i+3,j+1,k+1)
				v2=fi(i+2,j+1,k+1)
				v3=fi(i+1,j+1,k+1)
				v4=fi(i  ,j+1,k+1)
				v5=fi(i-1,j+1,k+1)

				dfx(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			elseif((i.eq.(l-2)) .and. (u(i,j,k).ge.0.d0))then !C III

				! Stencil 1 (3+2)
				v1=fi(i-2,j+1,k+1)
				v2=fi(i-1,j+1,k+1)
				v3=fi(i  ,j+1,k+1)
				v4=fi(i+1,j+1,k+1)
				v5=fi(i+2,j+1,k+1)

				dfx(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			else

 		                dfx(i,j,k)=( fi(i,j+1,k+1) + fi(i+1,j+1,k+1) )/2.d0 
 
			endif

		enddo
	enddo
enddo
!$OMP END PARALLEL DO


!---------------------------- Y DIRECTION---------------------------------------

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,v1,v2,v3,v4,v5)
do i=1,l-1
	do j=1,m
		do k=1,n-1

			if((j.ge.4) .and. (j.le.(m-3)))then !C I

				if(v(i,j,k).ge.0.d0)then
					! Stencil 1 (3+2)
					v1=fi(i+1,j-2,k+1)
					v2=fi(i+1,j-1,k+1)
					v3=fi(i+1,  j,k+1)
					v4=fi(i+1,j+1,k+1)
					v5=fi(i+1,j+2,k+1)

				else
					! Stencil 2 (2+3)
					v1=fi(i+1,j+3,k+1)
					v2=fi(i+1,j+2,k+1)
					v3=fi(i+1,j+1,k+1)
					v4=fi(i+1,  j,k+1)
					v5=fi(i+1,j-1,k+1)

				endif

				dfy(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			elseif((j.eq.3) .and. (v(i,j,k).lt.0.d0))then !C II

				! Stencil 2 (2+3)
				v1=fi(i+1,j+3,k+1)
				v2=fi(i+1,j+2,k+1)
				v3=fi(i+1,j+1,k+1)
				v4=fi(i+1,  j,k+1)
				v5=fi(i+1,j-1,k+1)

				dfy(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			elseif((j.eq.(m-2)) .and. (v(i,j,k).ge.0.d0))then !C III

				! Stencil 1 (3+2)
				v1=fi(i+1,j-2,k+1)
				v2=fi(i+1,j-1,k+1)
				v3=fi(i+1,  j,k+1)
				v4=fi(i+1,j+1,k+1)
				v5=fi(i+1,j+2,k+1)

				dfy(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			else

 		                dfy(i,j,k)=( fi(i+1,j,k+1) + fi(i+1,j+1,k+1) )/2.d0 
 
			endif

		enddo
	enddo
enddo
!$OMP END PARALLEL DO


!---------------------------- Z DIRECTION---------------------------------------

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,v1,v2,v3,v4,v5)
do i=1,l-1
	do j=1,m-1
		do k=1,n

			if((k.ge.4) .and. (k.le.(n-3)))then !C I

				if(w(i,j,k).ge.0.d0)then
					! Stencil 1 (3+2)
					v1=fi(i+1,j+1,k-2)
					v2=fi(i+1,j+1,k-1)
					v3=fi(i+1,j+1,k  )
					v4=fi(i+1,j+1,k+1)
					v5=fi(i+1,j+1,k+2)
				else
					! Stencil 2 (2+3)
					v1=fi(i+1,j+1,k+3)
					v2=fi(i+1,j+1,k+2)
					v3=fi(i+1,j+1,k+1)
					v4=fi(i+1,j+1,k  )
					v5=fi(i+1,j+1,k-1)
				endif

				dfz(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			elseif((k.eq.3) .and. (w(i,j,k).lt.0.d0))then !C II

				! Stencil 2 (2+3)
				v1=fi(i+1,j+1,k+3)
				v2=fi(i+1,j+1,k+2)
				v3=fi(i+1,j+1,k+1)
				v4=fi(i+1,j+1,k  )
				v5=fi(i+1,j+1,k-1)

				dfz(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			elseif((k.eq.(n-2)) .and. (w(i,j,k).ge.0.d0))then !C III

				! Stencil 1 (3+2)
				v1=fi(i+1,j+1,k-2)
				v2=fi(i+1,j+1,k-1)
				v3=fi(i+1,j+1,k  )
				v4=fi(i+1,j+1,k+1)
				v5=fi(i+1,j+1,k+2)

				dfz(i,j,k)= WENO5(v1,v2,v3,v4,v5)

			else

				dfz(i,j,k)=( fi(i+1,j+1,k) + fi(i+1,j+1,k+1) )/2.d0
			endif
		
		enddo
	enddo
enddo
!$OMP END PARALLEL DO


!$OMP PARALLEL WORKSHARE
FORALL (i = 2:l, j = 2:m, k = 2:n)

	RHS(i,j,k)=  ( u(i  ,j-1,k-1)*dfx(i  ,j-1,k-1) - u(i-1,j-1,k-1)*dfx(i-1,j-1,k-1) )/dx &
		    +( v(i-1,j  ,k-1)*dfy(i-1,j  ,k-1) - v(i-1,j-1,k-1)*dfy(i-1,j-1,k-1) )/dy &
		    +( w(i-1,j-1,k  )*dfz(i-1,j-1,k  ) - w(i-1,j-1,k-1)*dfz(i-1,j-1,k-1) )/dz

END FORALL 
!$OMP END PARALLEL WORKSHARE


! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************

	deallocate(dfx,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(dfy,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(dfz,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	
! ************************************************************************
! ************************************************************************

return
end subroutine WENOAppoxConvection


! #########################################################################
! #########################################################################

function WENO5(v1,v2,v3,v4,v5)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************

!	Function arguments
!	-----------------------------------------------------------------------------+
	implicit none

	double precision			::	WENO5

	double precision			::	v1,v2,v3,v4,v5
	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision, 	parameter 	:: 	epslon = 1.e-7
	double precision			::	t1,t2,t3
	double precision			::	s1,s2,s3
	double precision			::	w1,w2,w3
	double precision			::	a1,a2,a3


! +======================================+
! | CALCULATIONS			 |
! +======================================+

  	t1= v1 - 2.d0*v2 +      v3
	t2= v1 - 4.d0*v2 + 3.d0*v3
	s1= 13.d0/12.d0*t1*t1 + 0.25d0*t2*t2

	t1= v2 - 2.d0*v3 + v4
	t2= v2           - v4
	s2= 13.d0/12.d0*t1*t1 + 0.25d0*t2*t2

	t1=      v3 - 2.d0*v4 + v5
	t2= 3.d0*v3 - 4.d0*v4 + v5
	s3= 13.d0/12.d0*t1*t1 + 0.25d0*t2*t2 
	
	a1=0.1d0/(epslon+s1)**2
	a2=0.6d0/(epslon+s2)**2
	a3=0.3d0/(epslon+s3)**2
	w1=a1/(a1+a2+a3)
	w2=a2/(a1+a2+a3)
	w3=a3/(a1+a2+a3)

	t1=  v1/3.d0 - 7.d0*v2/6.d0 + 11.d0*v3/6.d0
	t2= -v2/6.d0 + 5.d0*v3/6.d0 +       v4/3.d0
	t3=  v3/3.d0 + 5.d0*v4/6.d0 -       v5/6.d0

	WENO5= w1*t1 + w2*t2 + w3*t3

end function WENO5

! #########################################################################
! #########################################################################

subroutine CahnHilliard &
(l,m,n,dx,dy,dz,dt,epn,Pe,rGd,rOd,fi,fiold,SpMF,u,v,w,Sm,num_threads,ConvPrevC,res,iteration,finew)

!**** IMPORTANT ****
! VERSION 1 according to notes (Dimensional Analysis summary and CH notes)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none
	
!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n

	double precision,	intent(in)	::	dx,dy,dz,dt
	double precision,	intent(in)	::	Pe,epn
	double precision,	intent(in)	::	rGd,rOd

	double precision,	intent(in)	::	fi   (l+1,m+1,n+1)
	double precision,	intent(in)	::	fiold(l+1,m+1,n+1)

	double precision,	intent(in)	::	SpMF   (l+1,m+1,n+1)

	double precision,	intent(in)	::	u(l  ,m-1,n-1)
	double precision,	intent(in)	::	v(l-1,m  ,n-1)
	double precision,	intent(in)	::	w(l-1,m-1,n  )

	double precision,	intent(in)	::	Sm   (l+1,m+1,n+1)

	integer,		intent(in)	::	num_threads

	double precision,	intent(inout)	::	ConvPrevC(l+1,m+1,n+1)
	double precision,	intent(inout)	::	finew(l+1,m+1,n+1)
	double precision,	intent(inout)	::	res
	integer,		intent(out)	::	iteration
	

!	Internal Variables
!	-----------------------------------------------------------------------------+
	integer 				::	i,j,k

	double precision, 	allocatable	::	RHS	(:,:,:)
	double precision, 	allocatable	::	bulkE	(:,:,:)
	double precision, 	allocatable	::	d2fi	(:,:,:)
	double precision, 	allocatable	::	source	(:,:,:)

	double precision,	parameter	::	nmda = 0.25d0
	double precision,	parameter	::	tao = 0.25d0

	double precision 			::	c1
	double precision 			::	a1,a2,a3,a4,a5,a6
	double precision 			::	temp1,temp2,temp3,temp4,temp5,rhoB

	double precision 			::	some
	double precision 			::	diagonal
	double precision 			::	epn2
	double precision,	parameter	::	alfa1 = 1.1d0
	double precision, 	parameter 	:: 	tol = 5.d-9
	integer, 		parameter 	::	iterNum = 18

	integer 				::	tid
	integer 				::	OMP_GET_THREAD_NUM

        double precision 			:: 	res_vec(0:num_threads-1)
	integer					::	alloc_stat


! +======================================+
! | CALCULATIONS			 |
! +======================================+
	
! ************************************************************************
! ALLOCATE ALL ARRAYS
! ************************************************************************

	allocate(RHS   (1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(source(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(d2fi  (1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(bulkE (1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

! ************************************************************************
! ************************************************************************


        epn2=epn*epn

	!$OMP PARALLEL WORKSHARE
	bulkE  = 0.d0
	d2fi   = 0.d0
	source = 0.d0
	RHS    = 0.d0
	!$OMP END PARALLEL WORKSHARE

	
	call WENOAppoxConvection(fi,u,v,w,l,m,n,dx,dy,dz,RHS)


!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
do i = 2,l
	do j = 2,m
		do k = 2,n

		bulkE(i,j,k)= (1.d0 - 3.d0*fi(i,j,k) + 2.d0*fi(i,j,k)*fi(i,j,k) )*fi(i,j,k)/2.d0
		
		d2fi(i,j,k)= &
			 ( fi(i+1,j  ,k  ) - 2.d0*fi(i,j,k) + fi(i-1,j  ,k  ) )/dx/dx &
			+( fi(i  ,j+1,k  ) - 2.d0*fi(i,j,k) + fi(i  ,j-1,k  ) )/dy/dy &
			+( fi(i  ,j  ,k+1) - 2.d0*fi(i,j,k) + fi(i  ,j  ,k-1) )/dz/dz
			
		source(i,j,k)= -ConvPrevC(i,j,k)

		enddo
	enddo
enddo 
!$OMP END PARALLEL DO



!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j)
do i=2,l
	do j=2,m

		! z direction
		
		! periodic
		!d2fi(i,j,1)=d2fi(i,j,n)
		!d2fi(i,j,n+1)=d2fi(i,j,2)
		!bulkE(i,j,1)=bulkE(i,j,n)
		!bulkE(i,j,n+1)=bulkE(i,j,2)

		! non-periodic
		bulkE(i,j,1)=bulkE(i,j,2)
		bulkE(i,j,n+1)=bulkE(i,j,n)
		d2fi(i,j,1)=d2fi(i,j,2)
		d2fi(i,j,n+1)=d2fi(i,j,n)
		
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,k)
do i=2,l
	do k=2,n

		! y direction
		
		! periodic
		!d2fi(i,1,k)=d2fi(i,m,k)
		!d2fi(i,m+1,k)=d2fi(i,2,k)
		!bulkE(i,1,k)=bulkE(i,m,k)
		!bulkE(i,m+1,k)=bulkE(i,2,k)

		! non-periodic
		d2fi(i,1,k)=d2fi(i,2,k)
		d2fi(i,m+1,k)=d2fi(i,m,k)
		bulkE(i,1,k)=bulkE(i,2,k)
		bulkE(i,m+1,k)=bulkE(i,m,k)
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(j,k)
do j=2,m
	do k=2,n
		
		! x direction
			
		! periodic
		! d2fi(1,j,k)=d2fi(l,j,k)
		! d2fi(l+1,j,k)=d2fi(2,j,k)
		! bulkE(1,j,k)=bulkE(l,j,k)
		! bulkE(l+1,j,k)=bulkE(2,j,k)

		! non-periodic
		d2fi(1,j,k)=d2fi(2,j,k)
		d2fi(l+1,j,k)=d2fi(l,j,k)
		bulkE(1,j,k)=bulkE(2,j,k)
		bulkE(l+1,j,k)=bulkE(l,j,k)
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,c1,a1,a2,a3,a4,a5,a6,temp1,temp2,temp3,temp4,temp5,rhoB)
do i=2,l
	do j=2,m
		do k=2,n
			c1 = (fi(i-1,j,k) + fi(i,j,k))/2.d0
			a1 = dsqrt(c1*c1*(1.d0-dabs(c1))*(1.d0-dabs(c1)))
			c1 = (fi(i+1,j,k) + fi(i,j,k))/2.d0
			a2 = dsqrt(c1*c1*(1.d0-dabs(c1))*(1.d0-dabs(c1)))
			c1 = (fi(i,j-1,k) + fi(i,j,k))/2.d0
			a3 = dsqrt(c1*c1*(1.d0-dabs(c1))*(1.d0-dabs(c1)))
			c1 = (fi(i,j+1,k) + fi(i,j,k))/2.d0
			a4 = dsqrt(c1*c1*(1.d0-dabs(c1))*(1.d0-dabs(c1)))
			if(k.eq.2)then 
				c1 = fi(i,j,k)
			else
				c1 = (fi(i,j,k-1) + fi(i,j,k))/2.d0
			endif
			a5 = dsqrt(c1*c1*(1.d0-dabs(c1))*(1.d0-dabs(c1)))
			c1 = (fi(i,j,k+1) + fi(i,j,k))/2.d0
			a6 = dsqrt(c1*c1*(1.d0-dabs(c1))*(1.d0-dabs(c1)))

			temp1 = &
				 ( a2*( bulkE(i+1,j  ,k  ) - bulkE(i  ,j  ,k  ) ) &
				  -a1*( bulkE(i  ,j  ,k  ) - bulkE(i-1,j  ,k  ) ) )/dx/dx &
				+( a4*( bulkE(i  ,j+1,k  ) - bulkE(i  ,j  ,k  ) ) &
				  -a3*( bulkE(i  ,j  ,k  ) - bulkE(i  ,j-1,k  ) ) )/dy/dy &
				+( a6*( bulkE(i  ,j  ,k+1) - bulkE(i  ,j  ,k  ) ) &
				  -a5*( bulkE(i  ,j  ,k  ) - bulkE(i  ,j  ,k-1) ) )/dz/dz


			temp2= &
				 ( d2fi(i+1,j  ,k  ) - 2.d0*d2fi(i,j,k) + d2fi(i-1,j  ,k  ) )/dx/dx &
				+( d2fi(i  ,j+1,k  ) - 2.d0*d2fi(i,j,k) + d2fi(i  ,j-1,k  ) )/dy/dy &
				+( d2fi(i  ,j  ,k+1) - 2.d0*d2fi(i,j,k) + d2fi(i  ,j  ,k-1) )/dz/dz


			temp3 = &
				 ( a2*( d2fi(i+1,j  ,k  ) - d2fi(i  ,j  ,k  ) ) &
				  -a1*( d2fi(i  ,j  ,k  ) - d2fi(i-1,j  ,k  ) ) )/dx/dx &
				+( a4*( d2fi(i  ,j+1,k  ) - d2fi(i  ,j  ,k  ) ) &
				  -a3*( d2fi(i  ,j  ,k  ) - d2fi(i  ,j-1,k  ) ) )/dy/dy &
				+( a6*( d2fi(i  ,j  ,k+1) - d2fi(i  ,j  ,k  ) ) &
				  -a5*( d2fi(i  ,j  ,k  ) - d2fi(i  ,j  ,k-1) ) )/dz/dz
		
			temp4 = (temp1 - epn2*temp3)/epn - nmda*tao/2.d0*d2fi(i,j,k)+ epn2*nmda/2.d0*temp2
			
			rhoB = 1.d0/(rGd*(1-SpMF(i,j,k))+rOd*SpMF(i,j,k))

			temp5 = temp4/Pe - RHS(i,j,k) - Sm(i,j,k)!*rhoB*(rOd-1.d0)/(1.d0-rhoB)

			ConvPrevC(i,j,k) = temp5
		
			source(i,j,k)= source(i,j,k) + temp5*2.d0
		
		enddo
	enddo
enddo
!$OMP END PARALLEL DO


a1 = 1.d0/(dx**2.d0) + 1.d0/(dy**2.d0) + 1.d0/(dz**2.d0)
a2 = 2.d0/(dx**4.d0) + 2.d0/(dy**4.d0) + 2.d0/(dz**4.d0)
diagonal=1.5d0 + nmda/2.d0/Pe*dt*( 2.d0*tao*a1 + epn2*(4.d0*a1*a1+a2) )

do iteration=1, iterNum
	
	res =0.d0

	!$OMP PARALLEL WORKSHARE
        FORALL (i = 2:l, j = 2:m, k = 2:n)
		
		! d2fi: grad2(C)
		d2fi(i,j,k)= &
			 ( finew(i+1,j,k) - 2.d0*finew(i,j,k) + finew(i-1,j,k) )/dx/dx &
			+( finew(i,j+1,k) - 2.d0*finew(i,j,k) + finew(i,j-1,k) )/dy/dy &
			+( finew(i,j,k+1) - 2.d0*finew(i,j,k) + finew(i,j,k-1) )/dz/dz

	END FORALL 
	!$OMP END PARALLEL WORKSHARE


	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j)
	do i=2,l
		do j=2,m
			!Non-Periodic in z
			d2fi(i,j,1  )= d2fi(i,j,2)
			d2fi(i,j,n+1)= d2fi(i,j,n)
		enddo
	enddo
	!$OMP END PARALLEL DO

	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,k)
	do i=2,l
		do k=2,n
			!Non-Periodic in y
			d2fi(i,1  ,k)= d2fi(i,2,k)
			d2fi(i,m+1,k)= d2fi(i,m,k)
		enddo
	enddo
	!$OMP END PARALLEL DO

	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(j,k)
	do j=2,m
		do k=2,n
			!Non-Periodic in x
			d2fi(1  ,j,k)= d2fi(2,j,k)
			d2fi(l+1,j,k)= d2fi(l,j,k)
		enddo
	enddo
	!$OMP END PARALLEL DO
             
	!$OMP PARALLEL WORKSHARE
	FORALL (i = 2:l, j = 2:m, k = 2:n)
		
		! bulkE: grad4(C)
		bulkE(i,j,k)= &
			 ( d2fi(i+1,j,k) - 2.d0*d2fi(i,j,k) + d2fi(i-1,j,k) )/dx/dx &
			+( d2fi(i,j+1,k) - 2.d0*d2fi(i,j,k) + d2fi(i,j-1,k) )/dy/dy &
			+( d2fi(i,j,k+1) - 2.d0*d2fi(i,j,k) + d2fi(i,j,k-1) )/dz/dz

	END FORALL 
	!$OMP END PARALLEL WORKSHARE


	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,some,res,tid,temp5)
	tid=OMP_GET_THREAD_NUM()
	res=0.d0

	!$OMP DO
	do i=2,l
		do j=2,m
			do k=2,n
				some =    2.d0*fi(i,j,k) &
					- 0.5d0*fiold(i,j,k) &
					- 1.5d0*finew(i,j,k) &
				       	+ dt*source(i,j,k) &
				       	+ dt*nmda/2.d0/Pe*(tao*d2fi(i,j,k)- epn2*bulkE(i,j,k)) 

				temp5 = finew(i,j,k)

				finew(i,j,k) = finew(i,j,k) + alfa1*some/diagonal
				
				if(finew(i,j,k).lt.0.d0)finew(i,j,k)=0.d0
				if(finew(i,j,k).gt.1.d0)finew(i,j,k)=1.d0

				res=dmax1(res,dabs(finew(i,j,k)-temp5)) 
			enddo
		enddo
	enddo
	!$OMP END DO
	
	res_vec(tid)=res
	!$OMP END PARALLEL


	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(j,k)
	do j=2,m
		do k=2,n
			! x direction
			! Non-periodic
			finew(1  ,j,k)=finew(2,j,k)
			finew(l+1,j,k)=finew(l,j,k)
		enddo
	enddo
	!$OMP END PARALLEL DO



	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,k)
	do i=2,l
		do k=2,n
			! y direction
			! Non-periodic
			finew(i,1,k)=finew(i,2,k)
			finew(i,m+1,k)=finew(i,m,k)
		enddo
	enddo
	!$OMP END PARALLEL DO


	!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j)
	do i=2,l
		do j=2,m
			! z direction
			! Non-periodic
			!finew(i,j,1)=finew(i,j,2) !This is the tricky one because it what controls the contact line
			finew(i,j,n+1)=finew(i,j,n)
		enddo
	enddo
	!$OMP END PARALLEL DO

	res=0.d0

	do tid=0,num_threads-1

		if(res_vec(tid).gt.res)then
			res=res_vec(tid)
		endif

	enddo
	
	if(res.lt.tol) go to 1
	
	
enddo

1	continue

! write(6,*) 'Iterations required to converge (CH) = ',iteration

! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************
 
	deallocate(RHS,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(source,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(d2fi,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(bulkE,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

! ************************************************************************
! ************************************************************************

return
end subroutine CahnHilliard

! #########################################################################
! #########################################################################

subroutine UpdateC(l,m,n,finew,fi,fiold)

	implicit none

	integer,		intent(in)	::	l,m,n
	double precision,	intent(in)	::	finew(l+1,m+1,n+1)
	double precision,	intent(inout)	::	fi(l+1,m+1,n+1)
	double precision,	intent(out)	::	fiold(l+1,m+1,n+1)

	integer					::	i,j,k

!$OMP PARALLEL DO SCHEDULE(STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
do i = 1,l+1
	do j = 1,m+1
		do k = 1,n+1

			fiold(i,j,k)= fi(i,j,k)
			fi(i,j,k)= finew(i,j,k)

		enddo
	enddo
enddo 
!$OMP END PARALLEL DO

return
end subroutine UpdateC

! #########################################################################
! #########################################################################

subroutine totc(iteration,dt,l,m,n,fi,dx,dy,dz)
      
	implicit none

	integer,		intent(in)	::	l,m,n,iteration
	double precision,	intent(in)	::	fi(l+1,m+1,n+1)
	double precision,	intent(in)	::	dt,dx,dy,dz
	
!	Internal Variables
!	-----------------------------------------------------------------------------+
	double precision			::	totVF,time
	integer					::	i,j,k
	character(len=50)			::	filename

totVF = 0.d0
time = iteration*dt

!$OMP PARALLEL DO REDUCTION(+:totVF) DEFAULT(SHARED) PRIVATE(i,j,k)	
do i = 2,l
	do j = 2,m
		do k = 2,n
			
			totVF = totVF + fi(i,j,k)*dx*dy*dz
		
		enddo
	enddo
enddo 
!$OMP END PARALLEL DO


	filename='results/inst_volume_fraction.dat'
	open(unit=522,file=filename,status='unknown',position='append')
	write(522,'(2(ES20.8E3))')time,totVF
	close(522)
	
return
end subroutine totc

! #########################################################################
! #########################################################################
