! The following subroutine is used to produce a n+1 level pressure
! field which satifies the continuity constraint.
subroutine Divergence(l,m,n,dx,dy,dz,dt,Re,rGd,rOd,u2,v2,w2,u3,v3,w3,Sm,SpMF,fi,p,num_threads,iteration,maxRes)

! +======================================+
! | DEFINITION				 |
! +======================================+

! VARIABLE DICTIONARY
! ***************************************************************************************
	implicit none

!	Subroutine Arguments
!	-----------------------------------------------------------------------------+
	integer,		intent(in)	::	l,m,n,num_threads

	double precision,	intent(in)	::	dx,dy,dz,dt
	double precision,	intent(in)	::	rOd,rGd,Re

	double precision,	intent(in)	::	u2(l  ,m-1,n-1)
	double precision,	intent(in)	::	v2(l-1,m  ,n-1)
	double precision,	intent(in)	::	w2(l-1,m-1,n  )

	double precision,	intent(inout)	::	u3(l  ,m-1,n-1)
	double precision,	intent(inout)	::	v3(l-1,m  ,n-1)
	double precision,	intent(inout)	::	w3(l-1,m-1,n  )

	double precision,	intent(in)	::	Sm  (l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMF(l+1,m+1,n+1)
	double precision,	intent(in)	::	fi  (l+1,m+1,n+1)
	double precision,	intent(inout)	::	p   (l+1,m+1,n+1)
	double precision,	intent(out)	::	maxRes
	integer,		intent(out)	::	iteration

!	Internal variables
!	-----------------------------------------------------------------------------+
	integer					::	i,j,k

	double precision, allocatable		::	res(:,:,:)
	double precision, allocatable		::	density(:,:,:)

	double precision			::	rho,temp,temp1,temp2,pmaxRes

	double precision, 	parameter 	:: 	relax = 1.2d0
	double precision, 	parameter 	:: 	tol = 0.005d0
	integer,	 	parameter 	:: 	maxit=299

	integer					::	tid,OMP_GET_THREAD_NUM,alloc_stat
	double precision			::	res_vec(0:num_threads-1)

	double precision			:: 	ub,vb,residual,diagonal
	double precision 			::	px1,px2,py1,py2,pz1,pz2
	double precision 			::	a1,a2,a3,a4,a5,a6,dx2,dy2,dz2

! +======================================+
! | CALCULATIONS			 |
! +======================================+

! ************************************************************************
! ALLOCATE ALL ARRAYS
! ************************************************************************

	allocate(res    (1:(l-1),1:(m-1),1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(density(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

! ************************************************************************
! ************************************************************************


dx2=dx*dx
dy2=dy*dy
dz2=dz*dz

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
do i=1,l-1
	do j=1,m-1
		do k=1,n-1
			res(i,j,k)=( (u3(i+1,j  ,k  )-u3(i,j,k))/dx &
				    +(v3(i  ,j+1,k  )-v3(i,j,k))/dy &
				    +(w3(i  ,j  ,k+1)-w3(i,j,k))/dz &
				    - Sm(i+1,j+1,k+1)*(rOd-1.d0) )/dt
			if(Re.lt.1.d0)res(i,j,k)=res(i,j,k)*Re
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,temp1,temp2)
do i=1,l+1
	do j=1,m+1
		do k=1,n+1
			temp1 = fi(i,j,k)
			temp2 = SpMF(i,j,k)
! 			if(temp1.lt.0.d0)temp1 = 0.d0
! 			if(temp1.gt.1.d0)temp1 = 1.d0
! 			if(temp2.lt.0.d0)temp2 = 0.d0
! 			if(temp2.gt.1.d0)temp2 = 1.d0

			density(i,j,k)= rho(rGd,rOd,temp1,temp2)
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!Solving the Pressure Poisson equation by SOR method 
do iteration = 1, maxit

	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,a1,px1,a2,px2,a3,py1,a4,py2,a5,a6,pz1,pz2,diagonal,residual,maxRes,tid)

	tid=OMP_GET_THREAD_NUM()
	maxRes = 0.d0

	!$OMP DO
	do k=2,n
		do j=2,m
			do i=mod(k+j+1,2)+2,l,2
				a1=(density(i,j,k)+density(i-1,j,k))/2.d0
				px1=(p(i,j,k)-p(i-1,j,k))/a1

				a2=(density(i,j,k)+density(i+1,j,k))/2.d0
				px2=(p(i+1,j,k)-p(i,j,k))/a2

				a3=(density(i,j,k)+density(i,j-1,k))/2.d0
				py1=(p(i,j,k)-p(i,j-1,k))/a3

				a4=(density(i,j,k)+density(i,j+1,k))/2.d0
				py2=(p(i,j+1,k)-p(i,j,k))/a4

				a5=(density(i,j,k)+density(i,j,k-1))/2.d0
				a6=(density(i,j,k)+density(i,j,k+1))/2.d0

				pz1=p(i,j,k)-p(i,j,k-1)/a5
				pz2=p(i,j,k+1)-p(i,j,k)/a6
				diagonal=(a1+a2)/(a1*a2)/dx2+(a3+a4)/(a3*a4)/dy2+(a5+a6)/(a5*a6)/dz2

				residual=res(i-1,j-1,k-1)-((px2-px1)/dx2+(py2-py1)/dy2+(pz2-pz1)/dz2)

				maxRes = dmax1(maxRes,dabs(residual))

				p(i,j,k)=p(i,j,k)-relax*residual/diagonal
			enddo
		enddo
	enddo
	!$OMP END DO
	
	res_vec(tid)=maxRes
	!$OMP END PARALLEL

	pmaxRes=0.d0
	
	do tid=0,num_threads-1
		!write(*,*) tid,res_vec(tid)
		if(res_vec(tid).gt.pmaxRes)then
		pmaxRes=res_vec(tid)
		endif
	enddo

	!Pressure boundary condition update
	!z direction
	do i=2,l
		do j=2,m
			p(i,j,1  ) = p(i,j,2)
			p(i,j,n+1) = p(i,j,n)
		enddo
	enddo
	
	!y direction
	do i=2,l
		do k=2,n
			p(i,1  ,k) = p(i,2,k)
			p(i,m+1,k) = p(i,m,k) 
		enddo
	enddo

	!x direction
	do k=2,n
		do j=2,m
			p(1  ,j,k) = p(2,j,k)
			p(l+1,j,k) = p(l,j,k)
		enddo
	enddo


	!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,a1,px1,a2,px2,a3,py1,a4,py2,a5,a6,pz1,pz2,diagonal,residual,maxRes,tid)

	tid=OMP_GET_THREAD_NUM()
	maxRes = pmaxRes

	!$OMP DO
	do k=2,n
		do j=2,m
			do i=mod(k+j,2)+2,l,2
				a1=(density(i,j,k)+density(i-1,j,k))/2.d0
				px1=(p(i,j,k)-p(i-1,j,k))/a1

				a2=(density(i,j,k)+density(i+1,j,k))/2.d0
				px2=(p(i+1,j,k)-p(i,j,k))/a2
	
				a3=(density(i,j,k)+density(i,j-1,k))/2.d0
				py1=(p(i,j,k)-p(i,j-1,k))/a3

				a4=(density(i,j,k)+density(i,j+1,k))/2.d0
				py2=(p(i,j+1,k)-p(i,j,k))/a4

				a5=(density(i,j,k)+density(i,j,k-1))/2.d0
				a6=(density(i,j,k)+density(i,j,k+1))/2.d0

				pz1=p(i,j,k)-p(i,j,k-1)/a5
				pz2=p(i,j,k+1)-p(i,j,k)/a6
				diagonal=(a1+a2)/(a1*a2)/dx2+(a3+a4)/(a3*a4)/dy2+(a5+a6)/(a5*a6)/dz2

				residual=res(i-1,j-1,k-1)-((px2-px1)/dx2+(py2-py1)/dy2+(pz2-pz1)/dz2)

				maxRes = dmax1(maxRes,dabs(residual))

				p(i,j,k)=p(i,j,k)-relax*residual/diagonal
			enddo
		enddo
	enddo
	!$OMP END DO
	res_vec(tid)=maxRes
	!$OMP END PARALLEL

	maxRes=0.d0

	do tid=0,num_threads-1
		if(res_vec(tid).gt.maxRes)then
			maxRes=res_vec(tid)
		endif
	enddo

	!Pressure boundary condition update
	!z direction
	do i=2,l
		do j=2,m
			p(i,j,1  ) = p(i,j,2)
			p(i,j,n+1) = p(i,j,n)
		enddo
	enddo
	
	!y direction
	do i=2,l
		do k=2,n
			p(i,1  ,k) = p(i,2,k)
			p(i,m+1,k) = p(i,m,k) 
		enddo
	enddo

	!x direction
	do k=2,n
		do j=2,m
			p(1  ,j,k) = p(2,j,k)
			p(l+1,j,k) = p(l,j,k)
		enddo
	enddo

	if(maxRes.le.tol) go to 1 

enddo     

1 continue

! Velocity updated using the n+1 level of pressure field

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,temp)
do i=1,l 
	do j=1,m-1
		do k=1,n-1
			temp=(density(i+1,j+1,k+1)+density(i,j+1,k+1))/2.d0
			if(Re>=1.d0)then
				u3(i,j,k)=u3(i,j,k)-dt*(p(i+1,j+1,k+1)-p(i,j+1,k+1))/dx/temp
			else
				u3(i,j,k)=u3(i,j,k)-dt*(p(i+1,j+1,k+1)-p(i,j+1,k+1))/dx/Re/temp
			endif
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,temp)
do i=1,l-1
	do j=1,m 
		do k=1,n-1
			temp=(density(i+1,j+1,k+1)+density(i+1,j,k+1))/2.d0
			if(Re>=1.)then
				v3(i,j,k)=v3(i,j,k)-dt*(p(i+1,j+1,k+1)-p(i+1,j,k+1))/dy/temp
			else 
				v3(i,j,k)=v3(i,j,k)-dt*(p(i+1,j+1,k+1)-p(i+1,j,k+1))/dy/Re/temp
			endif
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k,temp)
do i=1,l-1
	do j=1,m-1
		do k=2,n !Note difference here 
			temp=(density(i+1,j+1,k+1)+density(i+1,j+1,k))/2.d0
			if(Re>=1.)then
				w3(i,j,k)=w3(i,j,k)-dt*(p(i+1,j+1,k+1)-p(i+1,j+1,k))/dz/temp
			else
				w3(i,j,k)=w3(i,j,k)-dt*(p(i+1,j+1,k+1)-p(i+1,j+1,k))/dz/Re/temp
			endif
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!z direction
do i=1,l-1
	do j=1,m-1
		w3(i,j,1)=0.d0
	enddo
enddo


!write(*,*)'iteration number=',iteration
!write(*,*)'Pressure maxRes=',maxRes

! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************
 
	deallocate(res,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(density,STAT=alloc_stat )
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

! ************************************************************************
! ************************************************************************

return
end subroutine Divergence

! #########################################################################
! #########################################################################

subroutine UpdateU(l,m,n,u3,v3,w3,u2,v2,w2)

	implicit none

	integer,		intent(in)	::	l,m,n
	double precision,	intent(out)	::	u2(l  ,m-1,n-1)
	double precision,	intent(in)	::	u3(l  ,m-1,n-1)
	double precision,	intent(out)	::	v2(l-1,m  ,n-1)
	double precision,	intent(in)	::	v3(l-1,m  ,n-1)
	double precision,	intent(out)	::	w2(l-1,m-1,n  )
	double precision,	intent(in)	::	w3(l-1,m-1,n  )

	integer					::	i,j,k


!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
do i=1,l
	do j=1,m-1
		do k=1,n-1
			u2(i,j,k)=u3(i,j,k)
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
do i=1,l-1
	do j=1,m
		do k=1,n-1
			v2(i,j,k)=v3(i,j,k)
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

!$OMP PARALLEL DO SCHEDULE (STATIC) DEFAULT(SHARED) PRIVATE(i,j,k)
do i=1,l-1
	do j=1,m-1
		do k=1,n
			w2(i,j,k)=w3(i,j,k)
		enddo
	enddo
enddo
!$OMP END PARALLEL DO

return
end subroutine UpdateU

