program mainprogram

! +======================================+
! | DEFINITION				 |
! +======================================+

	implicit none
	
	integer, parameter		::	l = 200
	integer, parameter		::	m = 200
	integer, parameter		::	n = 100

	double precision		::	Hdrop,Rdrop
	double precision		::	Lx,Ly,Lz
	double precision		::	dx,dy,dz,dt
	double precision		::	Re,Pr,Ja,Sc,Pe,epn,We,Fr,Bod
	double precision		::	rGd,rGv,rGk,rGcp,rGb
	double precision		::	rOd,rOv,rOk,rOcp,rOb
	double precision		::	rMW,Pabs,psA,psB,psC

	double precision, allocatable	::	u2(:,:,:),v2(:,:,:),w2(:,:,:)
	double precision, allocatable	::	u3(:,:,:),v3(:,:,:),w3(:,:,:)
	double precision, allocatable	::	preU(:,:,:),preV(:,:,:),preW(:,:,:)
	double precision, allocatable	::	p(:,:,:),pot(:,:,:)

	double precision, allocatable	::	Sm(:,:,:)

	double precision, allocatable	::	fi(:,:,:)
	double precision, allocatable	::	fiold(:,:,:)
	double precision, allocatable	::	finew(:,:,:)
	double precision, allocatable	::	ConvPrevC(:,:,:)

	double precision, allocatable	::	SpMF(:,:,:)
	double precision, allocatable	::	SpMFnew(:,:,:)
	double precision, allocatable	::	SpMFold(:,:,:)
	double precision, allocatable	::	ConvPrevS(:,:,:)

	double precision, allocatable	::	T(:,:,:)
	double precision, allocatable	::	Tnew(:,:,:)
	double precision, allocatable	::	Told(:,:,:)
	double precision, allocatable	::	ConvPrevT(:,:,:)
	
	double precision, parameter	::	filimmin = 0.01d0
	double precision, parameter	::	filimmax = 0.5d0

	integer				::	iteration, prevIt,lastIt
	integer				::	num_threads,OMP_GET_NUM_THREADS
	integer				::	alloc_stat
	integer				::	i,j,k

	double precision		::	resC,resS,resT,resU,resP
	integer				::	nitC,nitS,nitT,nitU,nitP

	integer		 		:: 	date_time(8)
	character*10			::	time_aux(3)
	integer(kind=8)			::	t1,t2,clock_rate,clock_max
	double precision		::	maxtime
	logical 			:: 	baksaved


! +======================================+
! | CALCULATIONS			 |
! +======================================+

maxtime = (47.d0*60.d0+50.d0)*60.d0

baksaved = .FALSE.

! Real wall-clock time
call system_clock(t1, clock_rate, clock_max)

! Preliminary check
if ((mod(l,2).ne.0).or.(mod(m,2).ne.0).or.(mod(n,2).ne.0))then
	write(6,*) "Error: The number of cells within the physcal domain is not an odd number in at least one direction."	
	STOP	
endif
 
! RUN DETAILS
! ************************************
	write(6,100) "JOB DETAILS"
	write(6,'(4x,a)') "Author: Pedro J. Saenz (p.saenz@ed.ac.uk)"
	write(6,'(4x,a)') "Institution: University of Edinburgh"
	
	call date_and_time(time_aux(1),time_aux(2),time_aux(3),date_time)
	write(6,'(4x,a,i2.2,a,i2.2,a,i4.4)') "Date: ",date_time(3),"/",date_time(2),"/",date_time(1)
	write(6,'(4x,a,i2.2,a,i2.2,a,i2.2,/)') "Time: ",date_time(5),":",date_time(6),":",date_time(7)
	
	
! PARALLEL RUN
! ************************************
	!$OMP PARALLEL
		num_threads = OMP_GET_NUM_THREADS()
	!$OMP END PARALLEL
	write(6,'(4x,a,i)') 'Num threads = ',num_threads


! ************************************************************************
! ALLOCATE ALL ARRAYS
! ************************************************************************
	write (6, 101) "Attempting to allocate all arrays..."

	allocate(u2  (1:l,1:(m-1),1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(u3  (1:l,1:(m-1),1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(preU(1:l,1:(m-1),1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	allocate(v2  (1:(l-1),1:m,1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(v3  (1:(l-1),1:m,1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(preV(1:(l-1),1:m,1:(n-1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	allocate(w2  (1:(l-1),1:(m-1),1:n),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(w3  (1:(l-1),1:(m-1),1:n),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(preW(1:(l-1),1:(m-1),1:n),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	
	allocate(p(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(pot(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	allocate(fi(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(fiold(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(finew(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP 
	allocate(ConvPrevC(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	allocate(Sm(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	allocate(SpMFold(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(SpMF(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(SpMFnew(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(ConvPrevS(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	allocate(Told(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(T(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(Tnew(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'
	allocate(ConvPrevT(1:(l+1),1:(m+1),1:(n+1)),STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Not enough memory **'

	write (6, 101) "Arrays allocated successfully"
	

! SIMULATION SETUP
! ************************************

	call parameter_setting (Hdrop,Rdrop,l,m,n,Lx,Ly,Lz,dx,dy,dz,dt,Re,Pr,Ja,Sc,Pe,epn,We,Fr,Bod,rGd,rGv,rGk,rGcp,rGb,rOd,rOv,rOk,rOcp,rOb,rMW,Pabs,psA,psB,psC)

	call data_load (l,m,n,dx,dy,dz,Hdrop,Rdrop,Lx,Ly,Lz,epn,Fr,rGd,rOd,psA,psB,psC,rMW,Pabs,fi,fiold,ConvPrevC,SpMF,SpMFold,ConvPrevS,T,Told,ConvPrevT,u2,v2,w2,preU,preV,preW,p,prevIt)
	
	write(6,100) "TIME STEPPING"
	

	! This is required to have a good approximate initial guess for the iterative methods
	finew = fi
	Tnew = T
	SpMFnew = SpMF
	u3 = u2
	v3 = v2
	w3 = w2


 	if (prevIt.eq.0) then
	 	!call totc(prevIt,dt,l,m,n,fi,dx,dy,dz)
		call totvapmass(prevIt,dt,l,m,n,fi,SpMF,rGd,rOd,dx,dy,dz)
		!call totenergy(prevIt,dt,l,m,n,dx,dy,dz,rGd,rGcp,rOd,rOcp,T,fi,SpMF)

		!call flow_output (l,m,n,dx,dy,dz,Lx,Ly,u3,v3,w3,p,finew,Tnew,SpMFnew,Sm,0)
		
		call drop_parameters(l,m,n,dx,dy,dz,dt,Lx,Ly,fi,Sm,0)
		
		call sliceYZ_output_GC(l,m,n,dy,dz,Ly,u3,v3,w3,p,fi,T,SpMF,Sm,0)

	endif
	

	lastIt = prevIt + 500000

	do iteration = prevIt+1, lastIt
		
		
		!SOLVE GOVERNING EQUATIONS
		!************************************
       
		call PhaseChange (l,m,n,dt,dz,rGd,rOd,rMW,Pabs,psA,psB,psC,filimmax,fi,SpMF,T,Sm)
		
		call CahnHilliard (l,m,n,dx,dy,dz,dt,epn,Pe,rGd,rOd,fi,fiold,SpMF,u2,v2,w2,Sm,num_threads,ConvPrevC,resC,nitC,finew)
		
		call Species (l,m,n,dx,dy,dz,dt,Re,Sc,rGd,rOd,u2,v2,w2,Sm,fi,SpMF,SpMFold,filimmin,filimmax,num_threads,ConvPrevS,resS,nitS,SpMFnew)
		
		call Temperature (l,m,n,dx,dy,dz,dt,Re,Pr,Ja,rGd,rGcp,rGk,rOd,rOcp,rOk,u2,v2,w2,Sm,fi,finew,SpMF,SpMFnew,T,Told,num_threads,ConvPrevT,resT,nitT,Tnew)

		call NS_solver (l,m,n,dx,dy,dz,dt,epn,Re,We,Fr,Bod,rGd,rGv,rGb,rOd,rOv,rOb,u2,v2,w2,u3,v3,w3,preU,preV,preW,p,finew,fi,SpMFnew,SpMF,Tnew,T,Sm*0.d0,num_threads,nitU,nitP,resU,resP)


		!UPDATE VARIABLES
		!************************************
		call UpdateC(l,m,n,finew,fi,fiold)
		call UpdateSpMF(l,m,n,SpMFnew,SpMF,SpMFold)
		call UpdateT(l,m,n,Tnew,T,Told)
		call UpdateU(l,m,n,u3,v3,w3,u2,v2,w2)

		!RESIDUALS & CHECKS
		!************************************
		if(mod(iteration,1).eq.0) then
			write(6,101) 'Timestep = ',iteration
			write(6,102) 'Max.Res. C = ',resC,'Max.Res. S = ',resS,'Max.Res. T = ',resT,'Max.Res. U = ',resU,'Max.Res. P = ',resP
			write(6,103) 'Numb.It. C = ',nitC,'Mumb.It. S = ',nitS,'Numb.It. T = ',nitT,'Numb.It. U = ',nitU,'Numb.It. P = ',nitP
		endif
		
		
		!SAVE DATA
		!************************************
		if(mod(iteration,50).eq.0) then

			call drop_parameters(l,m,n,dx,dy,dz,dt,Lx,Ly,fi,Sm,iteration)

			call totc(iteration,dt,l,m,n,fi,dx,dy,dz)
			call totenergy(iteration,dt,l,m,n,dx,dy,dz,rGd,rGcp,rOd,rOcp,T,fi,SpMF)
			call totvapmass(iteration,dt,l,m,n,fi,SpMF,rGd,rOd,dx,dy,dz)
		endif


		! Standard output
		if(mod(iteration,10000).eq.0) then

			call flow_output (l,m,n,dx,dy,dz,Lx,Ly,u3,v3,w3,p,finew,Tnew,SpMFnew,Sm,iteration)

		endif

		! Slice output
		if((mod(iteration,400).eq.0)) then

			call sliceYZ_output_GC(l,m,n,dy,dz,Ly,u3,v3,w3,p,finew,Tnew,SpMFnew,Sm,iteration)

		endif

		! Real wall-clock time
		call system_clock(t2, clock_rate, clock_max)
		if(mod(iteration,50).eq.0) then
			
			call date_and_time(time_aux(1),time_aux(2),time_aux(3),date_time)
			write(6,'(4x,a,i2.2,a,i2.2,a,i4.4)') "Date: ",date_time(3),"/",date_time(2),"/",date_time(1)
			write(6,'(4x,a,i2.2,a,i2.2,a,i2.2,/)') "Time: ",date_time(5),":",date_time(6),":",date_time(7)
			
			write(*,*) 'Elapsed real time (s) = ', real(t2-t1)/real(clock_rate)
		endif
		
		
		! Backup
		!if(mod(iteration,lastIt).eq.0) then
		!	call backup (l,m,n,u,v,w,p,fi,fiold,ConvPrevC,SpMF,SpMFold,ConvPrevS,T,ConvPrevT,iteration)
		!endif

		if(((real(t2-t1)/real(clock_rate)).gt.maxtime).and.(.not.(baksaved))) then
			call backup (l,m,n,u2,v2,w2,preU,preV,preW,p,fi,fiold,ConvPrevC,SpMF,SpMFold,ConvPrevS,T,Told,ConvPrevT,iteration)
			baksaved=.TRUE.
			go to 1

		endif

		

	enddo


1	continue

write(6,100) "STOPPING SIMULATION"
	
! ************************************************************************
! DEALLOCATE ALL ARRAYS
! ************************************************************************
	write(6,101) "Attempting to deallocate all arrays..."

	deallocate(u2,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(u3,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(preU,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	deallocate(v2,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(v3,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(preV,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	deallocate(w2,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(w3,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(preW,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	
	deallocate(p,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(pot,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	deallocate(fi,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(fiold,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(finew,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(ConvPrevC,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	deallocate(Sm,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	deallocate(SpMFold,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(SpMF,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(SpMFnew,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(ConvPrevS,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	deallocate(Told,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(T,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(Tnew,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'
	deallocate(ConvPrevT,STAT=alloc_stat)
		if ( alloc_stat .ne. 0 ) STOP '** Unsuccessful deallocation **'

	write(6,101) "Arrays deallocated successfully"


! ************************************************************************
! ************************************************************************

	write(6,100) "END"
	
100   format(/,a)      
101   format(/,4x,a,I8)
102   format(5(8x,a,ES14.6E3,/))
103   format(5(8x,a,I3,/))

end program mainprogram


subroutine check_limits(l,m,n,num_threads,fi,T,SpMF)

	integer,		intent(in)	::	l,m,n,num_threads
	double precision,	intent(in)	::	fi(l+1,m+1,n+1)
	double precision,	intent(in)	::	T(l+1,m+1,n+1)
	double precision,	intent(in)	::	SpMF(l+1,m+1,n+1)

	integer					::	i,j,k,tid,OMP_GET_THREAD_NUM
	double precision			::	maxdev_fi,maxdev_T,maxdev_SpMF
	double precision			::	dev_vec_fi(0:num_threads-1)
	double precision			::	dev_vec_T(0:num_threads-1)
	double precision			::	dev_vec_SpMF(0:num_threads-1)

!$OMP PARALLEL DEFAULT(SHARED) PRIVATE(i,j,k,maxdev_fi,maxdev_T,maxdev_SpMF,tid)
tid = OMP_GET_THREAD_NUM()
maxdev_fi = 0.d0
maxdev_T = 0.d0
maxdev_SpMF = 0.d0

!$OMP DO
do i = 2,l
	do j = 2,m
		do k = 2,n

			! Checking fi limits
			if(fi(i,j,k).lt.0.d0) then
				maxdev_fi = max(maxdev_fi,-fi(i,j,k))
								
			else if(fi(i,j,k).gt.1.d0)then
				maxdev_fi = max(maxdev_fi,fi(i,j,k)-1.d0)
								
			endif

			! Checking T limits
			if(T(i,j,k).lt.0.d0) then
				maxdev_T = max(maxdev_T,-T(i,j,k))
								
			else if(T(i,j,k).gt.1.d0)then
				maxdev_T = max(maxdev_T,T(i,j,k)-1.d0)
								
			endif

			! Checking SpMF limits
			if(SpMF(i,j,k).lt.0.d0) then
				maxdev_SpMF = max(maxdev_SpMF,-SpMF(i,j,k))
								
			else if(SpMF(i,j,k).gt.1.d0)then
				maxdev_SpMF = max(maxdev_SpMF,SpMF(i,j,k)-1.d0)
								
			endif

		enddo
	enddo
enddo 
!$OMP END DO
dev_vec_fi(tid)=maxdev_fi
dev_vec_T(tid)=maxdev_T
dev_vec_SpMF(tid)=maxdev_SpMF
	
!$OMP END PARALLEL

maxdev_fi = 0.d0
maxdev_T = 0.d0
maxdev_SpMF = 0.d0

do tid=0,num_threads-1
	if(dev_vec_fi(tid).gt.maxdev_fi)then
		maxdev_fi=dev_vec_fi(tid)
	endif

	if(dev_vec_T(tid).gt.maxdev_T)then
		maxdev_T=dev_vec_T(tid)
	endif

	if(dev_vec_SpMF(tid).gt.maxdev_SpMF)then
		maxdev_SpMF=dev_vec_SpMF(tid)
	endif
enddo

write(6,*) "Checking if the variables are out their limits... "
write(6,*) "C max deviation    = ",maxdev_fi
write(6,*) "T max deviation    = ",maxdev_T
write(6,*) "SpMF max deviation = ",maxdev_SpMF
			
if (maxdev_fi.gt.1.d-10) then
	write(6,*) "The maximum deviation of C seems too large."
!  	STOP
endif	

if (maxdev_T.gt.1.d-10) then
	write(6,*) "The maximum deviation of T seems too large."
! 	STOP
endif	

if (maxdev_SpMF.gt.1.d-10) then
	write(6,*) "The maximum deviation of SpMF seems too large."
! 	STOP
endif	

end subroutine check_limits
