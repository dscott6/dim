
function []=pp_evap_profile()

clc
clear
clf
close all

global m
global n

%--------------------------------------------------------------------------
% CONFIGURATION
%--------------------------------------------------------------------------


m=200;
n=130;

% Data set
firstdat = 0;
lastdat = 100;
datint = 20;

% Basename
fbasename = 'slice_YZ_';

%--------------------------------------------------------------------------
% PROGRAM
%--------------------------------------------------------------------------

h=figure;
set(h, 'Visible','off');

for k=firstdat:datint:lastdat
    
    % display(strcat('drawing ',num2str(k),'th file'))
    % filename=files(k).name;

    filename=strcat(fbasename,num2str(k),'.dat');
    display(filename)
    [X,Y,Z,Header]=import_sliceYZ_data(filename,varpointer);
    [fv,p] = contourf(X,Y,Z,30);
    
    % Properties of the PLOT within the figure controlled by p
    set(p,'Edgecolor','none')
    axis equal
    axis tight
    xlabel(Header{1})
    ylabel(Header{2})
    th = title(strcat('t = ',num2str(k)),'HorizontalAlignment','Left');
    tpos = get(th,'position') ;
    tpos(1) = min(min(X)); %Reset title position to the left
    set(th,'position',tpos);

    % Properties of the FIGURE which contains the plot controlled by h,gcf,gaf
    set(gcf,'Color',[1 1 1])
    
    % Color bar (It works like another plot within the figure
    % We can have a xlabel, ylable, Title...
    cbar_handle=colorbar();
    set(get(cbar_handle,'Title'),'string',Header{3})
   % caxis([0, 1])
    colormap(jet)
    
    
    % Save plots in .fig and .png files   
    figfilename=strcat(prefix,'fig_',fbasename,num2str(k),'.fig');
    saveas(h,figfilename)

    figfilename=strcat(prefix,'fig_',fbasename,num2str(k),'.png');
    saveas(h,figfilename)

    clf
    
end

end

%**************************************************************************
%**************************************************************************
%**************************************************************************

function [X,Y,Z,Header]=import_sliceYZ_data(filename,varpointer)

global l
global m
global n


% Number of lines before the first set of data

mm=(l+1)*(m+1)*(n+1);

delimiterIn = ' ';
headlines = 3;
A = importdata(filename,delimiterIn,headlines);

if size(A.data,1)~=(m+1)*(n+1)
    error('The number of points read does not match the number of points expected')
end

X=reshape(A.data(:,1),m+1,n+1);
Y=reshape(A.data(:,2),m+1,n+1);
Z=reshape(A.data(:,varpointer+2),m+1,n+1);
Header{1}='Y';
Header{2}='Z';

if varpointer == 1
    Header{3}='u';
elseif varpointer == 2
    Header{3}='v';
elseif varpointer == 3
    Header{3}='w';
elseif varpointer == 4
    Header{3}='fi';
elseif varpointer == 5
    Header{3}='p';
elseif varpointer == 6
    Header{3}='T';
elseif varpointer == 7
    Header{3}='mf';
elseif varpointer == 8
    Header{3}='Sm';
end

end
